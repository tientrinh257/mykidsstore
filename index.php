<?php
error_reporting(0);
session_start();
$session = session_id();
@define('_template', './templates/');
@define('_source', './sources/');
@define('_lib', './libraries/');
@define(_upload_folder, './media/upload/');
include_once _lib . "config.php";
// https://demo.hasthemes.com/mozar-preview/mozar/index-3.html
//Lưu ngôn ngữ chọn vào $_SESSION
$lang_arr = array("vi", "en", "cn", "ge");
if (isset($_GET['lang']) == true) {
    if (in_array($_GET['lang'], $lang_arr) == true) {
        $lang = $_GET['lang'];
        $_SESSION['lang'] = $lang;
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
	//$lang = $config["lang_default"];
} else {

    $lang = $config["lang_default"];
}

require_once _source . "lang_$lang.php";
include_once _lib . "constant.php";
include_once _lib . "functions.php";
include_once _lib . "functions_giohang.php";
include_once _lib . "library.php";
include_once _lib . "class.database.php";
include_once _lib . "file_requick.php";	
include_once _source . "counter.php";
include_once _source . "useronline.php";
include_once _source . "cart.php";


	$d = new database($config['database']);
	if (isset($_REQUEST['command']) && $_REQUEST['command'] == 'add' && $_REQUEST['productid'] > 0) {
		$pid = $_REQUEST["productid"];
		$q = isset($_GET['quality']) ? ($_GET['quality']) : "1";
		addtocart($pid, $q);
		redirect(base_url("gio-hang"));
	}
	$d->reset();
    $sql = "select url,banner_$lang as photo from #_banner where hienthi=1 and com='banner_giua'";
    $d->query($sql);
    $popup = $d->fetch_array();
?>
<!Doctype html>
<html  xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" lang="vi">
    <head  prefix="og: http://ogp.me/ns#; dcterms: http://purl.org/dc/terms/#">
        <?php include _template."layout/head.php";?>    
    </head>

    <body>
        <?php include _template."layout/header.php";?>
        <?php include _template . $template . "_tpl.php"; ?>
        <?php include _template."layout/footer.php";?>
		<?php include _template."layout/script.php";?>
    </body>
</html>
