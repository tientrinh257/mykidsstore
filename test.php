<?php 
    session_start(); 
    require("test_connection.php"); 
    if(isset($_GET['page'])){ 
          
        $pages=array("test_products", "test_cart"); 
          
        if(in_array($_GET['page'], $pages)) { 
              
            $_page=$_GET['page']; 
              
        }else{ 
              
            $_page="test_products"; 
              
        } 
          
    }else{ 
          
        $_page="test_products"; 
          
    } 
  
?> 
<!DOCTYPE html> 
  
<html> 
<head> 
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    <title>Shopping cart</title> 
    <link rel="stylesheet" href="test.css">
</head>

<body> 
      
    <div id="container"> 
  
        <div id="main"> 
              
            <?php require($_page.".php"); ?> 
  
        </div><!--end of main--> 
          
        <div id="sidebar"> 
        <h1>Cart</h1> 
            <?php 
            
                if(isset($_SESSION['cart'])){ 
                    
                    $sql="SELECT * FROM products WHERE id_product IN ("; 
                    
                    foreach($_SESSION['cart'] as $id => $value) { 
                        $sql.=$id.","; 
                    } 
                    
                    $sql=substr($sql, 0, -1).") ORDER BY name ASC"; 
                    $query=mysql_query($sql); 
                    while($row=mysql_fetch_array($query)){ 
                        
                    ?> 
                        <p><?php echo $row['name'] ?> x <?php echo $_SESSION['cart'][$row['id_product']]['quantity'] ?></p> 
                    <?php 
                        
                    } 
                ?> 
                    <hr /> 
                    <a href="test.php?page=test_cart">Go to cart</a> 
                <?php 
                    
                }else{ 
                    
                    echo "<p>Your Cart is empty. Please add some products.</p>"; 
                    
                } 
            
            ?>
        </div><!--end of sidebar--> 
  
    </div><!--end container--> 
  
</body> 
</html>
<?php 

echo "<pre>";
print_r($_SESSION);
echo "</pre>";

?>