<?php
$d->reset();
$sql = "select banner_vi from #_banner where com='banner_top'";
$d->query($sql);
$logo = $d->fetch_array();
$typeparent = isset($_GET['typeparent']) ? $_GET['typeparent'] : "";
$typechild = isset($typechild) ? $typechild : "";
$com = isset($com) ? $com : "";
?>


<div class="logo"> <a href="../" target="_blank" ><img src="<?= base_url()?>img/logo/logo.png" style="width:100px;"  alt="" /> </a></div>
<div class="sidebarSep mt0"></div>
<!-- Left navigation -->
<ul id="menu" class="nav">
    <li class="dash" id="menu1"><a class=" active" title="" href="index.php"><span>Trang chủ</span></a></li>
	<li class="categories_li<?php if ($typeparent == 'product' || $typechild == 'product' || $com == 'city' || $com == 'contact' || $com == 'order') echo ' activemenu' ?>" id="menu_danhmucsanpham"><a href="" title="" class="exp"><span>Danh Mục sản phẩm</span><strong></strong></a>
        <ul class="sub">
            <li <?php if ($com == "product" && ($_GET['act'] == 'man_list' || $_GET["act"] == "edit_list" )) echo ' class="this"' ?>><a href="index.php?com=product&act=man_list&typeparent=product">Danh mục cấp 1</a></li>
			<li <?php if ($com == "product" && ($_GET['act'] == 'man_cat' || $_GET["act"] == "edit_cat" )) echo ' class="this"' ?>><a href="index.php?com=product&act=man_cat&typeparent=product">Danh mục cấp 2</a></li>			
			<!-- <li <?php if ($com == "product" && ($_GET['act'] == 'man_item' || $_GET["act"] == "edit_item" )) echo ' class="this"' ?>><a href="index.php?com=product&act=man_item&typeparent=product">Danh mục cấp 3</a></li> -->
            <li <?php if ($com == 'product' && ($_GET["act"] == "man" || $_GET["act"] == "edit" )) echo ' class="this"' ?>><a href="index.php?com=product&act=man&typechild=product">Danh Sách Sản Phẩm</a></li>
			<!-- <li <?php if ($typeparent == 'goiuudai' && ($_GET["act"] == "man_list" || $_GET["act"] == "edit" )) echo ' class="this"' ?>><a href="index.php?com=news&act=man_list&typeparent=goiuudai">Loại sản phẩm</a></li>	 -->
			<li <?php if ($com == 'donhang') echo ' class="this"' ?>><a href="index.php?com=order&act=man">Đơn hàng</a></li> 
			<!-- <li <?php if ($typechild == 'thanhtoan') echo ' class="this"' ?> ><a href="index.php?com=info&act=capnhat&typechild=thanhtoan">Thông tin chuyển khoản</a></li> -->
        </ul>
    </li>
	

	
    <li class="hidden_log categories_li<?php if ($com == 'download') echo ' activemenu' ?>" id="menu_danhmucdownload"><a href="" title="" class="exp"><span>Danh Mục Download</span><strong></strong></a>
        <ul class="sub">
            <li <?php if ($typeparent == "download" && ($_GET['act'] == 'man_list' || $_GET["act"] == "edit_list" )) echo ' class="this"' ?>><a href="index.php?com=download&act=man_list&typeparent=download">Danh Mục Cấp 1</a></li> 
            <li <?php if ($typechild == 'download' && ($_GET["act"] == "man" || $_GET["act"] == "edit" )) echo ' class="this"' ?>><a href="index.php?com=download&act=man&typechild=download">Danh Sách Download</a></li>
        </ul>
    </li>
	
	
    <li class="article_li<?php if (( $typechild == 'gioithieu' || $typechild == 'video') && $typechild != 'dichvu') echo ' activemenu' ?>" id="menu_baiviet"><a href="#" title="" class="exp"><span>Danh Mục Bài Viết</span><strong></strong></a>
        <ul class="sub">
		
		    <!-- <li <?php if ($typechild == 'gioithieu') echo ' class="this"' ?> ><a href="index.php?com=info&act=capnhat&typechild=gioithieu">Giới thiệu</a></li> -->
	  		
			<!-- <li <?php if ($typechild == 'duan' && ($_GET["act"] == "man" || $_GET["act"] == "edit" )) echo ' class="this"' ?>><a href="index.php?com=news&act=man&typechild=duan">Công trình</a></li> -->
	
			
			<li <?php if ($typechild == 'news') echo ' class="this"' ?> ><a href="index.php?com=news&act=man&typechild=news">Tin tức</a></li>
			<!-- <li <?php if ($typechild == 'dichvu') echo ' class="this"' ?> ><a href="index.php?com=news&act=man&typechild=dichvu">Dịch vụ</a></li> -->
			<!-- <li <?php if ($typechild == 'uudai') echo ' class="this"' ?> ><a href="index.php?com=news&act=man&typechild=uudai">Báo giá</a></li> -->
			<!-- <li <?php if ($typechild == 'tuyendung') echo ' class="this"' ?> ><a href="index.php?com=news&act=man&typechild=tuyendung">Tuyển dụng</a></li> -->
			<!-- <li <?php if ($typechild == 'video') echo ' class="this"' ?> ><a href="index.php?com=video&act=man&typechild=video">Video Clip</a></li> -->
	
        </ul>

    </li>

  	<li class="hide_tinhtrang template_li<?php if ($typechild == 'lienhe' || $typechild == 'footer') echo ' activemenu' ?>" id="menu_trangtinh"><a href="#" title="" class="exp"><span>Trang tĩnh</span><strong></strong></a>
        <ul class="sub">
            <li <?php if ($typechild == 'footer') echo ' class="this"' ?> ><a href="index.php?com=info&act=capnhat&typechild=footer">Footer</a></li>    
            <li <?php if ($typechild == 'lienhe') echo ' class="this"' ?> ><a href="index.php?com=info&act=capnhat&typechild=lienhe">Liên hệ</a></li> 
        </ul>
    </li>
    <li class="hide_tinhtrang template_li" id="menu_trangtinh"><a href="#" title="" class="exp"><span>Thống kê lượt xem</span><strong></strong></a>
        <ul class="sub">
            <li><a href="index.php?com=thongke&act=luotxem_cap1">Thống kê lượt xem danh mục 1</a></li>
            <li><a href="index.php?com=thongke&act=luotxem_cap2">Thống kê lượt xem danh mục 2</a></li>
            <li><a href="index.php?com=thongke&act=luotxem">Thống kê lượt xem sản phẩm</a></li>
        </ul>
    </li>

    <!-- <li class="gallery_li<?php if ($com == "banner" || $com == "background" || $com == "image_url" || $com == "support_online" || $typechild == "bando") echo ' activemenu' ?>" id="menu6"><a href="#" title="" class="exp"><span>Hình Ảnh - Support </span><strong></strong></a>
        <ul class="sub">
            <li <?php if ($typechild == 'bg_header') echo ' class="this"' ?>><a href="index.php?com=background&act=capnhat&typechild=bg_header">Background thông kê </a></li>
            <li <?php if ($typechild == 'bg_footer') echo ' class="this"' ?>><a href="index.php?com=background&act=capnhat&typechild=bg_footer">Banner mobile</a></li>
            <li <?php if ($_GET['act'] == 'capnhat' && $com == "banner") echo ' class="this"' ?>><a href="index.php?com=banner&act=capnhat">Cập nhật Logo Top </a></li>
			<li <?php if ($_GET['act'] == 'man_banner' && $com == "banner") echo ' class="this"' ?>><a href="index.php?com=banner&act=man_banner">Cập nhật banner </a></li>
			<li <?php if ($_GET['act'] == 'man_phai' && $com == "banner") echo ' class="this"' ?>><a href="index.php?com=banner&act=man_phai">Cập nhật banner mobile</a></li>
            <li <?php if ($typechild == 'slider') echo ' class="this"' ?>><a href="index.php?com=image_url&act=man_photo&typechild=slider">Slide Show </a></li>
			<li <?php if ($typechild == 'mangxahoift') echo ' class="this"' ?>><a href="index.php?com=image_url&act=man_photo&typechild=mangxahoift">Mạng xã hội footer </a></li>
			<li <?php if ($com == 'support_online') echo ' class="this"' ?>><a href="index.php?com=support_online&act=man&typechild=support_online">Hỗ trợ trực tuyến</a></li>	
			<li <?php if ($com == 'bct') echo ' class="this"' ?>><a href="index.php?com=image_url&act=man_photo&typechild=bct">Hình bộ công thương</a></li>
        </ul>
    </li> -->

    <li class="setting_li<?php if (isset($com) && $com == 'setting' || $com == 'database' || $com == 'backup' || $com == 'user' || $com == 'newsletter') echo ' activemenu' ?>" id="menu8"><a href="#" title="" class="exp"><span>Cấu hình website</span><strong></strong></a>

        <ul class="sub">
            <li><a href="../ajax/sitemap.php" title="">Tạo sitemap</a></li>
			     
		    <!-- <li <?php if (isset($com) && $com == 'newsletter') echo ' class="this"' ?>><a href="index.php?com=newsletter&act=man" title="">Newsletter</a></li> -->
				
            <li <?php if (isset($com) && $com == 'setting') echo ' class="this"' ?>><a href="index.php?com=setting&act=capnhat" title="">Cấu hình chung</a></li>

            <li <?php if (isset($com) && $com == 'user' && $_GET['act'] == 'admin_edit') echo ' class="this"' ?>><a href="index.php?com=user&act=admin_edit">Thông tin Tài khoản</a></li>
        </ul>
    </li>
</ul>