<!--top-->  
<div class="home-three-wrapper">
    <!--Slider Area Start-->
    <div class="slider-area-home-three">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="preview-2">
                        <div id="nivoslider" class="slides">	
                            <a href="#"><img src="<?= base_url() ?>img/slider/slider-1.jpg" alt="" title="#slider-1-caption1"/></a>
                            <a href="#"><img src="<?= base_url() ?>img/slider/slider-6.jpg" alt="" title="#slider-1-caption1"/></a>
                        </div> 
                        <div id="slider-1-caption1" class="nivo-html-caption nivo-caption">
                            <div class="timethai" style=" 
                                position:absolute;
                                bottom:0;
                                left:0;
                                background-color:rgba(224, 53, 80, 0.6);
                                height:3px;
                                -webkit-animation: myfirst 5000ms ease-in-out;
                                -moz-animation: myfirst 5000ms ease-in-out;
                                -ms-animation: myfirst 5000ms ease-in-out;
                                animation: myfirst 5000ms ease-in-out;
                                ">
                            </div>
                            <!-- <div class="banner-content slider-1">
                                <div class="text-content">
                                    <h1 class="title1"><span>BỘ SƯU TẬP</span></h1>										
                                    <h2 class="title2" ><span>MỚI NHẤT NĂM NAY</span></h2>																	
                                    <h3 class="title3">Luôn hoàn thiện và đổi mới mỗi ngày để thành công hơn</h3>																																					<div class="banner-readmore">
                                        <a href="#" title="Read more">Xem Thêm</a>	
                                    </div>
                                </div>
                            </div> -->
                        </div>		  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End of Slider Area-->
    <!--Timer Product Carousel Area Start-->
    <div class="timer-product-carousel-area">
        <div class="container">
            <div class="section-padding">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title"><h2>Mẫu Mới</h2></div>
                    </div>
                </div>
                <div class="row">
                    <div class="product-carousel">
                        <?php
                            $a_sid=array();
                            if(isset($_SESSION['ss'])) {
                                foreach($_SESSION['ss'] as $k=>$v)
                                {
                                    $a_sid[$k]=$v['ssid'];
                                }
                            }
                            for ($j = 0, $count_spmoi = count($productweb); $j < $count_spmoi; $j++) { ?> 
                        <div class="col-lg-12">
                            <!--item 1 start-->
                            <div class="single-product-item">
                                <div class="single-product clearfix">
                                    <a href="<?= base_url(). $productweb[$j]["tenkhongdau_$lang"] ?>">
                                        <span class="product-image">
                                            <img alt="<?= $productweb[$j]['tenkhongdau_vi'] ?>" src="<?= base_url() ?>thumb/540x728/1/<?php
                                                    if ($productweb[$j]['photo'] != NULL)
                                                                echo _upload_product_l . $productweb[$j]['photo'];
                                                        else
                                                            echo 'images/no-image-available.png'; ?>"
                                                            alt="<?= $productweb[$j]["ten_$lang"] ?>" />
                                        </span>
                                    </a>
                                </div>
                                <h2 class="single-product-name"><a href="<?= base_url(). $productweb[$j]["tenkhongdau_$lang"] ?>"><?= $productweb[$j]["ten_$lang"] ?></a></h2>
                                
                                <div class="price-box">
                                    <p class="old-price">
                                        <span class="price"><?=$productweb[$j]["gia_vnd"] ?> vnđ</span>
                                    </p>
                                    <p class="special-price">
                                        <span class="price" style="color:#f36e25"><?= get_price($productweb[$j]["id"]) ?> vnđ</span>
                                    </p> 											
                                </div>
                                
                            </div><!--end of item 1-->
                        </div><!--end of col-md-3-->
                        <?php } ?>
                        
                        

                    </div>
                </div>
            </div>    
        </div>
    </div>
    <!--End of Timer Product Carousel Area-->
    
    <!--Product Area Start-->
    <div class="product-area-home-three">
        <div class="container">
            <div class="section-top-padding"> 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title"><h2>ĐỒ BÉ GÁI</h2></div>
                    </div>
                </div>
                <div class="product row">
                        <?php
                        $a_sid=array();
                        if(isset($_SESSION['ss'])) {
                            foreach($_SESSION['ss'] as $k=>$v)
                            {
                                $a_sid[$k]=$v['ssid'];
                            }
                        }
                        for ($j = 0, $count_spmoi = count($productgirl); $j < $count_spmoi; $j++) { ?> 
                    
                    <div class="col-xl-2 col-lg-3 col-md-4 col-12">
                        <div class="single-product-item">
                            <div class="sale-product-label"><span>new</span></div>
                            <div class="single-product clearfix">
                                <a href="<?= base_url(). $productweb[$j]["tenkhongdau_$lang"] ?>">
                                    <span class="product-image">
                                        <img alt="<?= $productgirl[$j]['tenkhongdau_vi'] ?>"  src="<?= base_url() ?>thumb/540x728/1/<?php
                                                    if ($productgirl[$j]['photo'] != NULL)
                                                                echo _upload_product_l . $productgirl[$j]['photo'];
                                                        else
                                                            echo 'images/no-image-available.png'; ?>"
                                                            alt="<?= $productgirl[$j]["ten_$lang"] ?>" />
                                    </span>
                                </a>
                            </div>
                            <h2 class="single-product-name"><a href="<?= base_url() . $productgirl[$j]["ten_$lang"] ?>"><?= $productgirl[$j]["ten_$lang"] ?></a></h2>
                            <div class="price-box">
                                    <p class="old-price">
                                        <span class="price"><?=$productgirl[$j]["gia_vnd"] ?> vnđ</span>
                                    </p>
                                    <p class="special-price">
                                        <span class="price" style="color:#f36e25"><?= get_price($productgirl[$j]["id"]) ?> vnđ</span>
                                    </p> 											
                                </div>
                        </div>
                    </div>
                        <?php } ?>
                    
                </div>
            </div>      
        </div>
    </div>
    <div class="product-area-home-three">
        <div class="container">
            <div class="section-top-padding"> 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title"><h2>ĐỒ BÉ NAM</h2></div>
                    </div>
                </div>
                <div class="product row">
                        <?php
                        $a_sid=array();
                        if(isset($_SESSION['ss'])) {
                            foreach($_SESSION['ss'] as $k=>$v)
                            {
                                $a_sid[$k]=$v['ssid'];
                            }
                        }
                        for ($j = 0, $count_spmoi = count($productboy); $j < $count_spmoi; $j++) { ?> 
                    
                    <div class="col-xl-2 col-lg-3 col-md-4 col-12">
                        <div class="single-product-item">
                            <div class="sale-product-label"><span>new</span></div>
                            <div class="single-product clearfix">
                                <a href="<?= base_url(). $productweb[$j]["tenkhongdau_$lang"] ?>">
                                    <span class="product-image">
                                        <img alt="<?= $productboy[$j]['tenkhongdau_vi'] ?>"  src="<?= base_url() ?>thumb/540x728/1/<?php
                                                    if ($productboy[$j]['photo'] != NULL)
                                                                echo _upload_product_l . $productboy[$j]['photo'];
                                                        else
                                                            echo 'images/no-image-available.png'; ?>"
                                                            alt="<?= $productboy[$j]["ten_$lang"] ?>" />
                                    </span>
                                </a>
                            </div>
                            <h2 class="single-product-name"><a href="<?= base_url() . $productboy[$j]["ten_$lang"] ?>"><?= $productboy[$j]["ten_$lang"] ?></a></h2>
                            <div class="price-box">
                                    <p class="old-price">
                                        <span class="price"><?=$productboy[$j]["gia_vnd"] ?> vnđ</span>
                                    </p>
                                    <p class="special-price">
                                        <span class="price" style="color:#f36e25"><?= get_price($productboy[$j]["id"]) ?> vnđ</span>
                                    </p> 											
                                </div>
                        </div>
                    </div>
                        <?php } ?>
                    
                </div>
            </div>      
        </div>
    </div>
</div>    
<!--End of Page Wrapper-->
        