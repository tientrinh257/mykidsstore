

<div class="row pd0 mg0 ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd0">
        <div class="title_right wow zoomInUp"><h2><?= $title_tcat ?></h2></div>
    </div>
</div>
<?php if (isset($_GET['idl'])) {?>
    <?php
    $a_sid=array();
    foreach($_SESSION['ss'] as $k=>$v){
      $a_sid[$k]=$v['ssid'];
    }
   
    if($_GET['com']=='tim-kiem' || $_GET['com']=='tag') {?>
    <?php if(count($product)<1) {?>
    <p style="font-weight: bold;">
        Không tìm thấy thông tin sản phẩm. <a href="san-pham.html">Click vào đây để đến trang Sản phẩm</a>
    </p>
    <?php }?>
    <?php }?>

<div class="row pd0 mg0 ">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd0">	
	
<div class="box_product box_spnb">
    <?php for ($j = 0, $count_spmoi = count($product); $j < $count_spmoi; $j++) { ?>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
			<div class="item_product wow flipInX" >
					<div class="box_km_hot">
						<?php
							if ($product[$j]['giamgia']>0){
								?>
							<div class="km_sp">-<?=$product[$j]['giamgia']?>%</div>
						<?php }?>
					
					
					</div>
                    <div class="zoom_product">
					<a href="<?= $product[$j]["tenkhongdau_$lang"] ?>">
                        <img src="thumb/280x230/1/<?php
                        if ($product[$j]['photo'] != NULL)
                            echo _upload_product_l . $product[$j]['photo'];
                        else
                            echo 'images/no-image-available.png';
                        ?>" alt="<?= $product[$j]["ten_$lang"] ?>" />
					</a>	
                    </div>
                   
				
					<div class="name_product">
							<h3>
								<?= $product[$j]["ten_$lang"] ?>
							</h3>
							<?php if($product[$j]["giamgia"]>0){?>
								<p > <?= number_format($product[$j]['gia_vnd']-(get_price_km($product[$j]['id'])*$product[$j]['gia_vnd']), 0, ",", ".") . " đ"; ?> 
									<span style="text-decoration: line-through;"><?php if ($product[$j]["gia_vnd"] == "") { ?> <?= _lienhe ?> <?php } else { ?> <?= number_format($product[$j]['gia_vnd'], 0, ",", ".") . " đ"; ?> <?php } ?></span>
									
								</p>
		
							<?php }else{?>
								<p><?php if ($product[$j]["gia_vnd"] <=0 ) { ?> <?= _lienhe ?> <?php } else { ?> <?= number_format($product[$j]['gia_vnd'], 0, ",", ".") . " đ"; ?> <?php } ?></p>
							<?php }?>
							
						</div>
                
			
                              
            </div><!--item_product-->
	</div>
<?php } ?>
    <div class="clear"></div>
    <div class="wrap_paging">
        <div class="paging paging_ajax clearfix"><?= pagesListLimit_layout($url_link, $totalRows, $pageSize, $offset) ?></div>
		
    </div><!--end wrap_paging-->
</div><!--box_product-->

</div>

</div>
<?php }else{?>
<div class="row pd0 mg0 ">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd0">	
	
<div class="box_product box_spnb">
    <?php 
	$d->reset();
	$sql = "select ten_$lang,tenkhongdau_$lang,id,photo from #_news_list where hienthi=1 and com='goiuudai' order by stt asc";
	$d->query($sql);
	$goiuudai = $d->result_array();
	for ($j = 0, $count_spmoi = count($goiuudai); $j < $count_spmoi; $j++) { ?>
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
			<div class="item_product wow flipInX" >
				
                    <div class="zoom_product">
					<a href="<?= $goiuudai[$j]["tenkhongdau_$lang"] ?>">
                        <img src="thumb/280x230/1/<?php
                        if ($goiuudai[$j]['photo'] != NULL)
                            echo _upload_news_l . $goiuudai[$j]['photo'];
                        else
                            echo 'images/no-image-available.png';
                        ?>" alt="<?= $goiuudai[$j]["ten_$lang"] ?>" />
					</a>	
                    </div>
                   
				
					<div class="name_product">
							<h3>
								<?= $goiuudai[$j]["ten_$lang"] ?>
							</h3>
						
							
						</div>
                
			
                              
            </div><!--item_product-->
	</div>
<?php } ?>
    
</div><!--box_product-->

</div>

</div>
<?php }?>

