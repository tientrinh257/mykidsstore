<?php

if(@$_REQUEST['command']=='delete' && $_REQUEST['pid']>0){
		remove_product($_REQUEST['pid']);
	}
	else if(@$_REQUEST['command']=='clear'){
		unset($_SESSION['cart']);
	}
	
	else if(@$_REQUEST['command']=='update'){

		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			$pid=$_SESSION['cart'][$i]['productid'];
			$q=intval($_REQUEST['product'.$pid]);
			
			
			
			if($q>0 && $q<=999){
				$_SESSION['cart'][$i]['qty']=$q;
			}
			else{
				$msg='Một số sản phẩm không cập nhật !, số lượng phải là một số giữa 1 và 999';
			}
		}
	}
?>

<!--Cart Main Area Start-->
<div class="cart-main-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Giỏ hàng</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12"> 
                <form name="form2" method="post">
                    <input type="hidden" name="pid" />
                    <input type="hidden" name="command" />
                    <table border="0" cellpadding="5px" cellspacing="1px" style="font-family:Verdana, Geneva, sans-serif; font-size: 11px;" width="100%">
                        <?php if(!empty($_SESSION['cart'])){ ?>
                            <tr class="bg-top-cart" >
                                <td class="text-center"><?= _stt ?></td>
                                <td class="text-center"><?= _masp ?></td>
                                <td class="text-center"><?= _ten ?></td>
                                <td class="text-center"><?= _hinhanh ?></td>        
                                <td class="text-center mobi_off"><?= _dongia ?></td>
                                <td class="text-center"><?= _soluong ?></td>
                                <td class="text-center mobi_off"><?= _thanhtien ?></td>
                                <td class="text-center"><?= _xoa ?></td>				
                            </tr>';
                            <?php foreach( $_SESSION['cart'] as $pid => $item){
                                $q=$item['qty'];
                                $pname=get_product_name($pid,$lang);
                                $pimg=get_product_img($pid);
                                $sori=$q;
                                $tongri=$q;
                                if($q==0) continue; ?>
                                <tr style="text-align: center; background-color:#fff">
                                    <td width="3%" class="text-center" style="height: 25px;"><?=$i+1?></td>
                                    <td width="10%"><?=get_product_code($pid)?></td>
                                    <td width="15%"><?=$pname?></td>
                                    <td width="10%"><img src="<?=_upload_product_l.$pimg?>" width="100" style="max-height:100px; margin-top:5px; margin-bottom:5px;"/></td>
                                
                            
                                    <td width="15%" class="text-center mobi_off"><?=number_format(get_price($pid),0, ',', '.')?>
                                &nbsp;VNĐ</td>
                            
                                    <td width="15%" class="text-center number-wrapper"><input type="number" data-sori="<?=$sori?>" data-id="<?=$pid?>" data-price="<?=get_price($pid)?>" data-color="<?=$color?>" maxlength="3" name="product<?=$pid?>" class="change_qty" value="<?=$q?>" maxlength="3" style="text-align:center; border:1px solid #d2d2d2 ;width: 40px ;height: 40px" />
                                &nbsp;</td>
                            
                                    <td width="40%" class="text-center price_tt mobi_off "><?=number_format(get_price($pid)*$tongri,0, ',', '.') ?> &nbsp;VNĐ</td>
                                    <td width="10%" class="text-center"><a href="javascript:del(<?=$pid?>)"><img src="images/delete.png" style="border:0" /></a></td>
                                </tr>
                            <?php } ?>
                        <tr>
                            <td colspan="10" style="background:#E6E6E6; height:20px; color: #666;text-align: right"><b>Tổng đơn hàng:
                                <span style="color: #F60;" class="get_order_total"><?=number_format(get_order_total(),0, ',', '.')?></span> &nbsp;VNĐ</b></td>

                        </tr>
                        <tr>
                            <td colspan="7" class="text-left" style="padding:10px 0 0 0">
                                <input class="btn btn-custom" type="button" value="<?=_tieptucmuahang?>" onclick="window.location=<?= base_url() ?>" class="button">
                                <?php if(!empty($_SESSION['cart']) and count($_SESSION['cart'])>0){ ?>
                                    <input class="btn btn-custom" type="btn" value="<?=_xoatatca?>" onclick="clear_cart()">
                                    <input class="btn btn-custom" type="button" value="<?=_capnhatgh?>" onclick="update_cart()">
                                    <input class="btn btn-custom" type="button" value="<?=_datmua?>" onclick="window.location='thanh-toan'">
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } else { ?>
                        <tr>
                            <td><?= _emptycart ?></td>
                        </tr> 
                    <?php } ?>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function del(pid){
        if(confirm('Bạn có thực sự muốn xóa mục này')){
            document.form2.pid.value=pid;
            document.form2.command.value='delete';
            document.form2.submit();
        }
    }
    function clear_cart(){
        if(confirm('Điều này sẽ làm cho giỏ hàng của bạn trống, tiếp tục không?')){
            document.form2.command.value='clear';
            document.form2.submit();
        }
    }
    function update_cart(){
        document.form2.command.value='update';
        document.form2.submit();
    }
    function price($val) {
        return parseInt($val.replace(/\./g, ""));
    }
</script>