<script type="text/javascript" src="js/my_script_check_form.js"></script>
<script type="text/javascript">
	function validEmail(obj) {
		var s = obj.value;
		for (var i = 0; i < s.length; i++)
			if (s.charAt(i) == " ") {
				return false;
			}
		var elem, elem1;
		elem = s.split("@");
		if (elem.length != 2)
			return false;

		if (elem[0].length == 0 || elem[1].length == 0)
			return false;

		if (elem[1].indexOf(".") == -1)
			return false;

		elem1 = elem[1].split(".");
		for (var i = 0; i < elem1.length; i++)
			if (elem1[i].length == 0)
				return false;
		return true;
	} //Kiem tra dang email
	function IsNumeric(sText) {
		var ValidChars = "0123456789";
		var IsNumber = true;
		var Char;

		for (i = 0; i < sText.length && IsNumber == true; i++) {
			Char = sText.charAt(i);
			if (ValidChars.indexOf(Char) == -1) {
				IsNumber = false;
			}
		}
		return IsNumber;
	} //Kiem tra dang so
	function check() {
		var frm = document.frm_order;

		if (frm.ten.value == '') {
			alert("<?= _nameError ?>");
			frm.ten.focus();
			return false;
		}
		if (frm.dienthoai.value == '') {
			alert("<?= _phoneError ?>");
			frm.dienthoai.focus();
			return false;
		}
		if (!IsNumeric(frm.dienthoai)) {
			alert('<?= _phoneErrorNH1 ?>');
			frm.dienthoai.focus();
			return false;
		}
		if (!isPhone(frm.dienthoai)) {
			alert('<?= _phoneErrorNH1 ?>');
			frm.dienthoai.focus();
			return false;
		}
		if (frm.diachi.value == '') {
			alert("<?= _addressError ?>");
			frm.diachi.focus();
			return false;
		}

		if (frm.email.value == '') {
			alert("<?= _emailError ?>");
			frm.email.focus();
			return false;
		}
		if (!validEmail(frm.email)) {
			alert('<?= _emailError1 ?>');
			frm.email.focus();
			return false;
		}
		frm.submit();
	}
</script>
<script language='javascript'>
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}


	$(document).on("click", 'input#httt', function () {
		if ($(this).val() == 2) {
			$('.act_nmg').addClass("act_block");
		} else {
			$('.act_nmg').removeClass("act_block");
		}
	});
</script>

<div class="cart-main-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2><?= _thanhtoan ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
			<form method="post" name="frm_order" action="" enctype="multipart/form-data" onsubmit="return check();">
				<div class="row pd0 mg0 ">

					<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pd0">

						<div class="block_donhang  width_common">
							<div class="title_donhang"><span><?=_thongtinnhanhang?></span></div>
						</div>
						<div class="text" style="padding:10px;border:1px solid #cacaca;margin-bottom:20px">
							<div class="step-content ">
								<div class="form-group">
									<div class="field required row">
										<!-- ko ifnot: element.label == '' -->
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-tn-12">
											<div class="txt_form">
												<b><?= _hoten ?></b>
												<span> *</span>
											</div>
										</div>
										<!-- /ko -->
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-tn-12">
											<div class="control" data-bind="css: {'_with-tooltip': element.tooltip}">
												<input type="text" name="ten" id="ten" class="form-control" value="<?= @$_SESSION['login_member']['hoten']?>" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row field required">
										<!-- ko ifnot: element.label == '' -->
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-tn-12">
											<div class="txt_form">
												<b><?= _dienthoai ?></b>
												<span> *</span>
											</div>
										</div>
										<!-- /ko -->
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-tn-12">
											<div class="control" data-bind="css: {'_with-tooltip': element.tooltip}">
												<input type="tel" name="dienthoai" id="dienthoai" class="form-control" value="<?= @$_SESSION['login_member']['phone'] ?>" onKeyPress="return isNumberKey(event)" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row field required">
										<!-- ko ifnot: element.label == '' -->
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-tn-12">
											<div class="txt_form">
												<b><?= _diachi ?></b>
												<span> *</span>
											</div>
										</div>
										<!-- /ko -->
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-tn-12">
											<div class="control" data-bind="css: {'_with-tooltip': element.tooltip}">
												<input type="text" name="diachi" id="diachi" class="form-control" value="<?= @$_SESSION['login_member']['address'] ?>" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row field required">
										<!-- ko ifnot: element.label == '' -->
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-tn-12">
											<div class="txt_form">
												<b>E-mail</b>
												<span> *</span>
											</div>
										</div>
										<!-- /ko -->
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-tn-12">
											<div class="control" data-bind="css: {'_with-tooltip': element.tooltip}">
												<input type="text" name="email" id="email" class="form-control" value="<?= @$_SESSION['login_member']['email'] ?>" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group note">
									<div class="row field">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-tn-12">
											<div class="txt_form">
												<b><?=_ghichu?></b>
											</div>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 col-tn-12">
											<textarea class="form-control" placeholder="Nhập ghi chú (nếu có)" name="noidung" cols="50" rows="5"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--end left_formtt-->
						<!--left_formhttt-->
						<div class="block_donhang  width_common">
							<div class="title_donhang"><span><?=_hinhthucthanhtoan?></span></div>
						</div>
						<div class="text" style="padding:10px;border:1px solid #cacaca;margin-bottom:20px">
							<div class="step-content">
								<div class="txt_form item_cod">
									<input type="radio" id="pay-cod" name="httt" value="1" checked="checked" style="cursor:pointer;" />
									<label for="pay-cod" style="cursor:pointer;">Thanh toán khi nhận hàng</label>
								</div>
								<div class="txt_form item_cod">
									<input type="radio" id="pay-atm" name="httt" value="2" style="cursor:pointer;" />
									<label for="pay-atm" style="cursor:pointer;">Chuyển khoản qua ngân hàng</label>
								</div>

								<div class="act_nmg">
									<?php 
										$d->reset();
										$sql_news = "select noidung_$lang from #_info where hienthi=1 and com='thanhtoan' ";
										$d->query($sql_news);
										$tb_thanhtoan = $d->fetch_array();
										echo $tb_thanhtoan["noidung_$lang"];
									?>
								</div>
							</div>
						</div>
						<!--end left_formhttt-->
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd_right0">
						<div class="block_donhang  width_common">
							<div class="title_donhang"><span>Đơn hàng</span>
								<a href="gio-hang" class="pull-right txt_back_cart txt_color_1">Sửa</a>
							</div>
						</div>
						<div class="text" style="padding:10px;border:1px solid #cacaca;margin-bottom:20px">
							<?php if(isset($_SESSION['cart'])){ 
				
								foreach($_SESSION['cart'] as $pid => $value) {
								$q = $value['qty'];
								$pname = get_product_name($pid,$lang);
								$pimg = get_product_img($pid);
								$tongri = $q;
								
							
								if($q==0) continue;
							?>

							<div class="item_list_donhang row">
								
								<div class="thumb_donhang col-md-4">
									<img src="<?= 'thumb/75x83/1/' ._upload_product_l . $pimg ?>" width="75" height="75" />
								</div>

								<div class="info_donhang col-md-4">

									<div class="title_sanpham_donhang"><?= $pname ?></div>

									<div class="qty">SL: <strong data-bind="text: $parent.qty"><?= $q ?></strong></div>

								</div>
								<div class="gia_thanh col-md-4 text-right">
									<span
										data-bind="text: getFormattedPrice($parent.row_total)"><?= number_format(get_price($pid), 0, ',', '.') ?>
										&nbsp;vnđ</span>
								</div>
								<div class="clearfix"></div>

							</div>

							<?php }?>
							<div class="item_list_donhang">
								<div class="row space_bottom_10">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
										Tạm tính:
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-3 text-right">
										<?= number_format(get_order_total(), 0, ',', '.') ?>&nbsp;đ</div>
									<div class="clearfix"></div>
								</div>

								<div class="row space_bottom_20 block_thanhtien">
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
										Thành tiền:<br>
										(Đã bao gồm VAT)
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 text-right">
										<div class="txt_giatien">
											<?= number_format(get_order_total(), 0, ',', '.') ?>&nbsp;vnđ</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<?php }?>
							<div style="margin-top: 15px">
								<button title='<?= _gui ?>' class="button button_thanhtoan" type="submit" name="next" style="cursor:pointer;"><span><?= _gui ?></span>
							</div>
						</div>
					</div>
				</div>
			</form>
			</div>
        </div>
    </div>
</div>
<!--end main_content_web-->