<?php
    if(@$_REQUEST['command']=='delete' && $_REQUEST['pid']>0){
		remove_product($_REQUEST['pid']);
	}
	else if(@$_REQUEST['command']=='clear'){
		unset($_SESSION['cart']);
	}
	
	else if(@$_REQUEST['command']=='update'){

		$max=count($_SESSION['cart']);
		foreach( $_SESSION as $pid => $item){
			$q=intval($_REQUEST['product'.$pid]);
			if($q>0 && $q<=999){
				$_SESSION['cart'][$pid]['qty']=$q;
			}
			else{
				$msg='Một số sản phẩm không cập nhật !, số lượng phải là một số giữa 1 và 999';
			}
		}
	}
?>
<!--Cart Main Area Start-->
<div class="cart-main-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Giỏ hàng</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form name="form2" method="post">
                    <input type="hidden" name="pid" />
                    <input type="hidden" name="command" />
                    <div class="cart-table table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <td class="p-index"><?= _stt ?></td>
                                    <th class="p-name"><?= _ten ?></th>
                                    <th class="p-image"><?= _hinhanh ?></th>
                                    <td class="p-code"><?= _masp ?></td>
                                    <th class="p-amount"><?= _dongia ?></th>
                                    <th class="p-quantity"><?= _soluong ?></th>
                                    <th class="p-total"><?= _thanhtien ?></th>
                                    <th class="p-times"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if( $cart_total_item > 0 ) { ?>
                                    <?php $i = 1; foreach ($_SESSION['cart'] as $pid => $value) {
                                        $product_detail = get_product_detail($pid);
                                        $sori=$value["qty"];
                                        $tongri=$value['qty']; ?>
                                        <tr>
                                            <td class="p-index"><?=$i?></td>
                                            <td class="p-name"><a href="<?= base_url($product_detail["tenkhongdau_$lang"]) ?>"><?= get_product_name($pid,$lang); ?></a></td>
                                            <td class="p-image">
                                                <a href="<?= base_url($product_detail["tenkhongdau_$lang"]) ?>"><img alt="" src="<?= 'thumb/75x83/1/' ._upload_product_l . $product_detail['photo'] ?>" class="floatleft"></a>
                                            </td>
                                            <td class="p-code"><?=get_product_code($pid)?></td>
                                            <td class="p-amount"><?=number_format(get_price($pid),0, ',', '.')?>&nbsp;VNĐ</td>
                                            <td class="p-quantity"><input class="change_qty" maxlength="12" type="text" value="<?= $value['qty'] ?>" data-sori="<?=$sori?>" data-id="<?=$pid?>" data-price="<?=get_price($pid)?>" name="product<?=$pid?>"></td>
                                            <td class="p-total price_tt mobi_off"><?=number_format(get_price($pid)*$tongri,0, ',', '.') ?> &nbsp;VNĐ</td>
                                            <td class="p-action"><a href="javascript:del(<?=$pid?>)"><i class="fa fa-times"></i></a></td>
                                        </tr>
                                    <?php $i++;} ?> 
                                    
                                <?php } else { ?>
                                    <tr>
                                        <td colspan="8"><?= _emptycart ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="all-cart-buttons">
                            <button class="button" type="button" onclick="window.location='<?= base_url() ?>'" ><span><?= _tieptucmuahang ?></span></button>
                            <div class="floatright">
                                <button class="button clear-cart" type="button" onclick="clear_cart()"><span><?= _xoatatca ?></span></button>
                                <!-- <button class="button" type="button" onclick="update_cart()><span><?php //echo _capnhatgh ?></span></button> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            
                        </div>
                        <div class="col-lg-4">
                            
                        </div>
                        <div class="col-lg-4">
                            <div class="amount-totals">
                                <!-- <p class="total">Subtotal <span>$156.87</span></p> -->
                                <p class="total">Tổng đơn hàng: <span style="color: #F60;" class="get_order_total"><?=number_format(get_order_total(),0, ',', '.')?></span> &nbsp;VNĐ</p>
                                <button class="button" type="button" onclick='window.location="<?= base_url() ."thanh-toan" ?>"'><span><?= _datmua ?></span></button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function del(pid){
        if(confirm('Bạn có thực sự muốn xóa mục này')){
            document.form2.pid.value=pid;
            document.form2.command.value='delete';
            document.form2.submit();
        }
    }
    function clear_cart(){
        if(confirm('Điều này sẽ làm cho giỏ hàng của bạn trống, tiếp tục không?')){
            document.form2.command.value='clear';
            document.form2.submit();
        }
    }
    function update_cart(){
        document.form2.command.value='update';
        document.form2.submit();
    }
    function price($val) {
        return parseInt($val.replace(/\./g, ""));
    }
</script>