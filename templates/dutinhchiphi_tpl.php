
<script type="text/javascript" src="js/my_script_check_form.js"></script>
<script type="text/javascript">
    function js_submit() {
		
		var buoc1_value = $('#buoc1_value');
		var buoc2_value = $('#buoc2_value');
		var buoc3_value = $('#buoc3_value');
		var buoc4_value0 = $('#buoc4_value0');
		var buoc4_value1 = $('#buoc4_value1');
		var buoc4_value2 = $('#buoc4_value2');
		var buoc5_value = $('#buoc5_value');
		
		var ten = $('#ten');
		var email = $('#email');
		var dienthoai = $('#dienthoai');
		
		if (buoc1_value.val() == '') {
			alert('<?= _banchuachonbuoc1?>');
			return false;
		}
		if (buoc2_value.val() == '') {
			alert('<?= _banchuachonbuoc2?>');
			return false;
		}
		if (buoc3_value.val() == '') {
			alert('<?= _banchuachonbuoc3?>');
			return false;
		}
		if (buoc4_value0.val() == '') {
			alert('<?= _banchuachonbuoc4a?>');
			return false;
		}
		if (buoc4_value1.val() == '') {
			alert('<?= _banchuachonbuoc4b?>');
			return false;
		}
		if (buoc4_value2.val() == '') {
			alert('<?= _banchuachonbuoc4c?>');
			return false;
		}
		if (buoc5_value.val() == '') {
			alert('<?= _banchuachonbuoc5?>');
			return false;
		}
		
		
		if (isEmpty(document.getElementById('ten'), "<?= _nameError ?>")) {
            document.getElementById('ten').focus();
            return false;
        }
	
		
		var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;
						
		if (email.val() == '') {
			email.focus();
			alert('<?= _emailError ?>');
			return false;
		}
		
		if (!emailRegExp.test(email.val())) {
			email.focus();
			alert('<?= _emailError1 ?>');
			return false;
		}

        if (isEmpty(document.getElementById('dienthoai'), "<?= _phoneError ?>")) {
            document.getElementById('dienthoai').focus();
            return false;
        }


        if (!isNumber(document.getElementById('dienthoai'), "<?= _phoneError1 ?>")) {
            document.getElementById('dienthoai').focus();
            return false;
        }
		var v = grecaptcha.getResponse();
            console.log("Resp" + v);
        if (v == '') {
               
				alert('Bạn chưa check chọn mã bảo vệ!');				
                return false;
        }
		
		$.ajax({
			url:"ajax/create_session.php",
			type:"POST",
			data:{buoc1_value:buoc1_value.val(),buoc2_value:buoc2_value.val(),buoc3_value:buoc3_value.val(),buoc4_value0:buoc4_value0.val(),buoc4_value1:buoc4_value1.val(),buoc4_value2:buoc4_value2.val(),buoc5_value:buoc5_value.val(),ten:ten.val(),email:email.val(),dienthoai:dienthoai.val()},
			async:true,
			dataType: "json",
			success:function(response){
				if(response['kt'] ==1){// thanh cong
					$('#myModalqick').modal('show').find('.modal-body').load('du-tinh-chi-phi');
				}
				else{
					return false;
				}
			}
		})
			
		
    }
</script>

<script language='javascript'>
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

</script>
<div class="row pd0 mg0 ">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <div class="title_right wow zoomInUp" style="background:none"><h2 style="color:#000"><?=_ngansachuoctinh?></h2></div>
    </div>
</div>
<div class="row pd0 mg0 ">

    <div id="tab_buoc" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 box_dutinh_1">
		<div class="item_buoc btn_b_1 clearfix" name="buoc1">
			<div><?=_buoc1?></div><p class=""><?=_loaicongtrinh?></p>
		</div>
		<div class="item_buoc btn_b_2 clearfix" name="buoc2">
			<div><?=_buoc2?></div><p class=""><?=_chatlieu?></p>
		</div>
		<div class="item_buoc btn_b_3 clearfix" name="buoc3">
			<div><?=_buoc3?></div><p class=""><?=_trangbihethong?></p>
		</div>
		<div class="item_buoc btn_b_4 clearfix" name="buoc4">
			<div><?=_buoc4?></div><p class=""><?=_noithat?></p>
		</div>
		<div class="item_buoc btn_b_5 clearfix" name="buoc5">
			<div><?=_buoc5?></div><p class=""><?=_thietkenoithat?></p>
		</div>
   
    </div>
	
	<div id="buoc1" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 box_dutinh_2 content_buoc">
		<div class="row pd0 mg0 ">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_text">
				<ul class="list_type btn_container">
					<?php for($i=0;$i<count($container);$i++){ ?>
						<li class="" rel="<?=$container[$i]["id"]?>">
							<a data-id="<?=$container[$i]["id"]?>"><?=$container[$i]["ten_$lang"]?></a>
						</li>
					<?php }?>
				</ul>
			</div>			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_hinh load_container">
			</div>
		</div>
    </div>
	<div id="buoc2" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 box_dutinh_2 content_buoc">
		<div class="row pd0 mg0 ">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_text">
				<ul class="list_type btn_chatlieu">
					<?php for($i=0;$i<count($chatlieu);$i++){ ?>
						<li class="" rel="<?=$chatlieu[$i]["id"]?>">
							<a data-id="<?=$chatlieu[$i]["id"]?>"><?=$chatlieu[$i]["ten_$lang"]?></a>
						</li>
					<?php }?>
				</ul>
			</div>			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_hinh load_chatlieu">
			</div>
		</div>
    </div>
	<div id="buoc3" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 box_dutinh_2 content_buoc">
		<div class="row pd0 mg0 ">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_text">
				<ul class="list_type btn_trangbi">
					<?php for($i=0;$i<count($trangbi);$i++){ ?>
						<li class="" rel="<?=$trangbi[$i]["id"]?>">
							<a data-id="<?=$trangbi[$i]["id"]?>"><?=$trangbi[$i]["ten_$lang"]?></a>
						</li>
					<?php }?>
				</ul>
			</div>			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_hinh load_trangbi">
			</div>
		</div>
    </div>
	<div id="buoc4" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 box_dutinh_2 content_buoc">
		<div class="row pd0 mg0 ">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_text">
				<?php 
					for($j=0;$j<count($noithat);$j++){
					$d->reset();
					$sql="select * from #_news where hienthi=1 and com='noithat' and id_list='".$noithat[$j]["id"]."'  order by stt,ngaytao desc";
					$d->query($sql);
					$noithat_con=$d->result_array();
				?>
				<script>
				$(document).ready(function (e) {
				$(".btn_noithat<?=$j?> li").click(function(){
					$(".btn_noithat<?=$j?> li").removeClass("act_b");
					$(this).addClass("act_b");
					var id = $(this).attr('rel');
		
					$('#buoc4_value<?=$j?>').val(id);
				
				})
				});
				</script>
				<p class="title_noithat"><?=$noithat[$j]["ten_$lang"]?></p>
				<ul class="list_type_noithat btn_noithat btn_noithat<?=$j?>">
					<?php for($i=0;$i<count($noithat_con);$i++){ ?>
						<li class="" rel="<?=$noithat_con[$i]["id"]?>">
							<a data-id="<?=$noithat_con[$i]["id"]?>"><?=$noithat_con[$i]["ten_$lang"]?></a>
						</li>
					<?php }?>
				</ul>
				<?php }?>
			</div>			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_hinh load_noithat">
			</div>
		</div>
    </div>
	<div id="buoc5" class="col-lg-8 col-md-8 col-sm-12 col-xs-12 box_dutinh_2 content_buoc">
		<div class="row pd0 mg0 ">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_text">
				<ul class="list_type btn_thietke">
					<?php for($i=0;$i<count($thietke);$i++){ ?>
						<li class="" rel="<?=$thietke[$i]["id"]?>">
							<a data-id="<?=$thietke[$i]["id"]?>"><?=$thietke[$i]["ten_$lang"]?></a>
						</li>
					<?php }?>
				</ul>
			</div>			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 box_dutinh_2_hinh load_thietke">
			</div>
		</div>
    </div>
</div>

<div class="box_xemketqua box_content">
            <div class="content"> 
			<div class="row pd0 mg0 ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
					<div class="title_right wow zoomInUp" style="background:none"><h2 style="color:#000"><?=_xemketquangay?></h2></div>
				</div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
				<input  name="buoc1_value" id="buoc1_value" type="hidden" class="form-control" size="50"  />
                <input  name="buoc2_value" id="buoc2_value" type="hidden" class="form-control" size="50"  />
				<input  name="buoc3_value" id="buoc3_value" type="hidden" class="form-control" size="50"  />
				<input  name="buoc4_value0" id="buoc4_value0" type="hidden" class="form-control" size="50"  />
				<input  name="buoc4_value1" id="buoc4_value1" type="hidden" class="form-control" size="50"  />
				<input  name="buoc4_value2" id="buoc4_value2" type="hidden" class="form-control" size="50"  /><input  name="buoc5_value" id="buoc5_value" type="hidden" class="form-control" size="50"  />				
						
                            <div class="form-group">
                            <!--    <label for="ten"><?= _hovaten ?></label>-->
                                <div class="input_item"><input placeholder="<?= _hovaten ?>" name="ten" type="text" class="form-control" id="ten" size="50" /></div><!--input_item-->
                            </div>                        
                          
                            <div class="form-group">
                            <!--    <label for="dienthoai"><?= _dienthoai ?></label>-->
                                <div class="input_item"><input placeholder="<?= _dienthoai ?>" name="dienthoai" type="text" class="form-control" id="dienthoai" size="50"/></div><!--input_item-->
                            </div>
                            <div class="form-group">
                             <!--   <label for="email"><?= _email ?></label>-->
                                <div class="input_item"><input placeholder="<?= _email ?>" name="email" id="email" type="text" class="form-control" size="50"  /></div><!--input_item-->
                            </div>                                                  
                          
							<div class="form-group">
							<div class="g-recaptcha" data-sitekey="6LcZ12kUAAAAACGjMy65sGR4uXV4SM2ozhsofsyL"></div>
							</div>
							
                            <div class="input_block nut_bt" >
                                <label>&nbsp;</label>
                                <div class="input_item" > 
							
                                    <input class="button" type="submit" value="<?= _gui?>" onclick="js_submit();" />
								
                                    
                                </div><!--input_item-->
                            </div>
                      
                   
           
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
				<?=$company_ketqua["noidung_$lang"]?>
				</div>
			</div>
	</div>		
</div>

<script>
$(".content_buoc[id^='buoc']").hide(); // Hide all content
    $("#tab_buoc .item_buoc:first").attr("id","current_buoc"); // Activate the first tab
    $(".content_buoc#buoc1").fadeIn(); // Show first tab's content
    
    $('#tab_buoc .item_buoc').click(function(e) {
        e.preventDefault();
        if ($(this).attr("id") == "current_buoc"){ //detection for current tab
         return;       
        }
        else{             
          $(".content_buoc").hide(); // Hide all content
          $("#tab_buoc .item_buoc").attr("id",""); //Reset id's
          $(this).attr("id","current_buoc"); // Activate this
          $('#' + $(this).attr('name')).fadeIn(); // Show content for the current tab
        }
    });
	
	 $(document).ready(function (e) {
		$(".btn_container li").click(function(){
			$(".btn_container li").removeClass("act_b");
			$(this).addClass("act_b");
			var id = $(this).attr('rel');
			$('#buoc1_value').val(id);
            $(".box_dutinh_2_hinh").load("ajax/loadhinh_spec.php", "id=" + id, function () {  
				callback_slick();	
            }
            );
            return false;
			
		})	
			
			$(".btn_chatlieu li").click(function(){
			$(".btn_chatlieu li").removeClass("act_b");
			$(this).addClass("act_b");
			var id = $(this).attr('rel');
			$('#buoc2_value').val(id);
				
			//$( ".box-hinh-loai" ).remove;
            $(".box_dutinh_2_hinh").load("ajax/loadhinh_spec.php", "id=" + id, function () {  
			
				callback_slick();
			
					
            }
            );
            return false;
		})		
			$(".btn_trangbi li").click(function(){
			$(".btn_trangbi li").removeClass("act_b");
			$(this).addClass("act_b");
			var id = $(this).attr('rel');
			$('#buoc3_value').val(id);
            $(".box_dutinh_2_hinh").load("ajax/loadhinh_spec.php", "id=" + id, function () {  
				callback_slick();	
            }
            );
            return false;
		})		
			$(".btn_noithat li").click(function(){
		
			var id = $(this).attr('rel');
            $(".box_dutinh_2_hinh").load("ajax/loadhinh_spec.php", "id=" + id, function () {  
				callback_slick();	
            }
            );
            return false;
		})		
			$(".btn_thietke li").click(function(){
			$(".btn_thietket li").removeClass("act_b");
			$(this).addClass("act_b");
			var id = $(this).attr('rel');
			$('#buoc5_value').val(id);
            $(".box_dutinh_2_hinh").load("ajax/loadhinh_spec.php", "id=" + id, function () {  
				callback_slick();	
            }
            );
            return false;
			
		})		
	}); 


	function callback_slick() {
				 $('.box-hinh-loai').slick({
					autoplay:true,
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
			
                    arrows:false,
                    responsive: [
                        {
                            breakpoint: 1124,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                infinite: true
                               
                            }
                        },
                        {
                            breakpoint: 800,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }

                    ]
                });
		}
		
</script>