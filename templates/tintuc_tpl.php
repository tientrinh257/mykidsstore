<!--Product Area Start-->
<div class="product-area-home-three">
    <div class="container">
        <div class="section-top-padding"> 
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title"><h2>TIN TỨC</h2></div>
                </div>
            </div>
            <div class="product row">
                <?php for ($i = 0, $count_tintuc = count($tintuc); $i < $count_tintuc; $i++) { ?> 
                    <div class="col-xl-2 col-lg-3 col-md-4 col-12">
                        <div class="single-product-item">
                            <div class="sale-product-label"><span>Mới</span></div>
                            <div class="single-product clearfix">
                                <a href="<?= $tintuc[$i]["tenkhongdau_$lang"] ?>">
                                    <span class="product-image">
                                        <img src="thumb/540x728/1/<?php
                                        if ($tintuc[$i]['photo'] != NULL)
                                            echo _upload_news_l . $tintuc[$i]['photo'];
                                        else
                                            echo 'images/no-image-available.png'; ?>"
                                            alt="<?= $tintuc[$i]["ten_$lang"] ?>" />
                                    </span>
                                </a>
                            </div>
                            <h2 class="single-product-name"><a href="#"><?= $tintuc[$i]["ten_$lang"] ?></a></h2>
                            
                        </div>
                    </div>
                <?php } ?>
                
            </div>
        </div>      
    </div>
</div>
<!--End of Product Area-->