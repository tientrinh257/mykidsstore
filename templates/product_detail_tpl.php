<!--Breadcrumb Start-->
<div class="breadcrumb-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="index">Sản Phẩm</a><span>/ </span></li>
                        <li><strong>Chi Tiết</strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Breadcrumb-->
<!--Product Details Area Start-->
<div class="product-deails-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="product-details-content row">
                    <div class="col-xl-7 col-lg-7 col-md-7">
                        <div class="zoomWrapper">
                            <div id="img-1" class="zoomWrapper single-zoom">
                                <a href="<?= base_url() ?>thumb/540x728/1/<?=_upload_product_l.$row_detail['photo']?>">
                                    <img id="zoom1" src="<?= base_url() ?>thumb/540x728/1/<?=_upload_product_l.$row_detail['photo']?>" data-zoom-image="thumb/540x728/1/<?=_upload_product_l.$row_detail['photo']?>" alt="big-1">
                                </a>
                            </div>
                            <div class="product-thumb row">
                                <ul class="p-details-slider" id="gallery_01">
                                    <?php if(count($album_hinh)>0){
                                        foreach ($album_hinh as $key => $value) { ?>
                                        <li class="col-lg-12">
                                            <a class="elevatezoom-gallery" href="<?= base_url() ?>thumb/540x728/1/<?=_upload_product_l.$row_detail['photo']?>" data-image="<?=_upload_product_l.$value['photo']?>" data-zoom-image="thumb/540x728/1/<?=_upload_product_l.$value['photo']?>"><img src="<?=_upload_product_l.$value['photo']?>" alt=""></a>
                                        </li>
                                    <?php } } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="product-details-conetnt">
                            <div class="shipping">
                                <div class="single-service">
                                    <span class="fa fa-truck"></span>
                                    <div class="service-text-container">
                                        <h3>GIAO HÀNG MIỄN PHÍ</h3>
                                        <p>Đơn hàng trên 1 triệu</p>
                                    </div>   
                                </div>
                                <div class="single-service">
                                    <span class="fa fa-cube"></span>
                                    <div class="service-text-container">
                                        <h3>Gói Quà Miễn Phí</h3>
                                        <p>Đơn hàng trên 500K</p>
                                    </div>   
                                </div>
                            </div>
                            <div class="product-name">
                                <h1> <?=$row_detail['ten_'.$lang]?> </h1>
                            </div>
                            <!-- <p class="no-rating no-margin"><a href="#">Bạn là người xem thứ 239</a></p> -->
                            <p class="availability">Tình Trạng: <span>Hàng Mới</span></p>
                            <div class="price-box">
                                <p class="old-price">
                                    <span class="price"><?=number_format($row_detail['gia_vnd'],0,",",".") ?></span> vnđ
                                </p>
                                <p class="special-price">
                                    <span class="price"><?= number_format(get_price($row_detail['id']),0,",",".") ?></span> vnđ
                                </p> 											
                            </div>
                            <div class="details-description">
                                <p><?=$row_detail['mota_'.$lang]?></p>
                            </div>
                            <div class="add-to-cart-qty">
                                <div class="cart-qty-button">
                                    <label for="qty">Số lượng:</label>
                                    <form method="post" id="form-add-cart">
                                        <input type="hidden" name="pid" value="<?= $row_detail['id'] ?>" />
                                        <input type="number" min="1" class="input-text qty" value="1" maxlength="12" id="qty" name="qty">
                                        <button class="button btn-cart" type="submit" name="submit"><span>Thêm vào giỏ</span></button>
                                    </form> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="p-details-tab">
                            <ul role="tablist" class="nav nav-tabs">
                                <li><a class="active" data-toggle="tab" role="tab" aria-controls="description" href="#description">Mô Tả Sản Phẩm</a></li>
                                
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tab-content review product-details">
                            <div id="description" class="tab-pane show active" role="tabpanel">
                                <p><?=$row_detail['mota_'.$lang]?> </p>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                
                <div class="product-carousel-area section-top-padding">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title"><h2>SẢN PHẨM LIÊN QUAN</h2></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="upsell-product-details-carousel">
                            <?php $a_sid=array();
                            if( isset($_SESSION['ss'])) {
                                foreach($_SESSION['ss'] as $k=>$v)
                                {
                                    $a_sid[$k]=$v['ssid'];
                                }
                            }
                            if( count($sanpham_khac) > 0) {

                            for ($j = 3, $count_spmoi = count($sanpham_khac); $j < $count_spmoi; $j++) { ?> 
                        
                                <div class="col-lg-12">
                                    <div class="single-product-item">
                                        <div class="single-product clearfix">
                                            <a href="<?= base_url() .$sanpham_khac[$j]["tenkhongdau_$lang"] ?>">
                                                <span class="product-image">
                                                    <img src="<?= base_url() ?>thumb/540x728/1/<?php
                                                    if ($sanpham_khac[$j]['photo'] != NULL)
                                                        echo _upload_product_l . $sanpham_khac[$j]['photo'];
                                                    else
                                                        echo 'images/no-image-available.png'; ?>"
                                                    alt='<?= $sanpham_khac[$j]["ten_$lang"] ?>' />
                                                </span>
                                            </a>
                                        </div>
                                        <h2 class="single-product-name"><a href="<?= base_url(). $sanpham_khac[$j]["ten_$lang"] ?>"><?= $sanpham_khac[$j]["ten_$lang"] ?></a></h2>
                                        <div class="price-box">
                                            <p class="old-price">
                                                <span class="price"><?= number_format($sanpham_khac[$j]['gia_vnd'],0,",",".")?></span> vnđ
                                            </p>
                                            <p class="special-price" style="color:#f36e25">
                                                <span class="price" ><?= number_format(get_price($sanpham_khac[$j]['id']),0,",",".") ?></span> vnđ
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php } }?>
                            
                        </div>
                    </div>
                </div>    
            </div>
            <!-- <div class="col-xl-3 col-lg-3">
                <div class="single-products-category">
                    <div class="section-title"><h2>SẢN PHẨM MỚI</h2></div>
                    <div class="category-products">
                        <?php $a_sid=array();
                        if(isset($_SESSION['ss'])) {
                            foreach($_SESSION['ss'] as $k=>$v)
                            {
                                $a_sid[$k]=$v['ssid'];
                            }
                        }
                        if( count($sanpham_khac) > 0) {
                        for ($j = 0, $count_spmoi = count($sanpham_khac); $j < 3; $j++) { ?> 
                    
                            <div class="product-items">
                                <div class="p-category-image">
                                    <a href="#">
                                        <img alt="" src="<?= base_url() ?>thumb/80x100/1/<?php
                                            if ($sanpham_khac[$j]['photo'] != NULL)
                                                echo _upload_product_l . $sanpham_khac[$j]['photo'];
                                            else
                                                echo 'images/no-image-available.png'; 
                                        ?>"
                                        alt='<?= $sanpham_khac[$j]["ten_$lang"] ?>' />
                                    </a>
                                </div>
                                <div class="p-category-text">
                                    <h2 class="category-product-name"><a href="<?= base_url() . $sanpham_khac[$j]["ten_$lang"] ?>"><?= $sanpham_khac[$j]["ten_$lang"] ?></a></h2>
                                    <div class="price-box">
                                        <p class="special-price">
                                            <span class="price"><?= $sanpham_khac[$j]['gia_vnd'] ?></span> vnđ
                                        </p> 											
                                    </div>
                                </div>
                            </div>
                        <?php } } ?>
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="section-title no-margin"><h2>Tags Tìm Kiếm</h2></div>
                    <div class="popular-tags">
                        <ul class="tag-list">                  
                            <li><a href="#">bộ Bé Trai</a></li>
                            <li><a href="#">bộ Bé Gái</a></li>
                            <li><a href="#">bộ mặc nhà</a></li>
                            <li><a href="#">bộ dã ngoại</a></li>
                            <li><a href="#">bộ lót bé gái</a></li>
                            <li><a href="#">bộ lót bé trai</a></li>
                            <li><a href="#">quần lót bé gái</a></li>
                            <li><a href="#">quần lót bé trai</a></li>
                            <li><a href="#">áo thun dây</a></li>
                            <li><a href="#">áo ba lỗ</a></li>
                            <li><a href="#">set mặc nhà</a></li>
                            <li><a href="#">set t-shirt</a></li>
                            <li><a href="#">set ba lỗ</a></li>
                        </ul>
                          <div class="tag-actions">
                            <a href="#">View All Tags</a>
                        </div>
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="section-title no-margin"><h2>ALBUMS MỚI </h2></div>
                    <div class="block-content">
                        <p class="empty">Cập nhật mẫu mới nhất hiện nay</p>
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="banner-box">
                        <a href="#"><img alt="" src="img/banner/14.jpg"></a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!--End of Product Details Area-->