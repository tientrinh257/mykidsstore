<!--Breadcrumb Start-->
<div class="breadcrumb-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><a href="tin-tuc">Tin Tức</a><span>/ </span></li>
                        <li><strong>Chi Tiết</strong></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Breadcrumb-->
<!--Product Details Area Start-->
<div class="product-deails-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-9">
                
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="p-details-tab">
                            <ul role="tablist" class="nav nav-tabs">
                                <li><a class="active" data-toggle="tab" role="tab" aria-controls="description" href="#description"><?=$tintuc_detail[0]['ten_'.$lang]?></a></li>
                                
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tab-content review product-details">
                            <div id="description" class="tab-pane show active" role="tabpanel">
                                <p><?=$tintuc_detail[0]["noidung_$lang"]?> </p>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
                
                <div class="product-carousel-area section-top-padding">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title"><h2>TIN TỨC LIÊN QUAN</h2></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="upsell-product-details-carousel">
                            <?php
$a_sid=array();
if( isset($_SESSION['ss'])) {
foreach($_SESSION['ss'] as $k=>$v)
    {
    $a_sid[$k]=$v['ssid'];
    }
}
for ($j = 0, $count_spmoi = count($tintuc_khac); $j < $count_spmoi; $j++) { ?> 
                            
                            <div class="col-lg-12">
                                <div class="single-product-item">
                                    <div class="single-product clearfix">
                                        <a href="<?= $tintuc_khac[$j]["tenkhongdau_$lang"] ?>">
                                            <span class="product-image">
                                                <img src="thumb/540x728/1/<?php
        if ($tintuc_khac[$j]['photo'] != NULL)
            echo _upload_news_l . $tintuc_khac[$j]['photo'];
        else
            echo 'images/no-image-available.png'; ?>"
alt="<?= $tintuc_khac[$j]["ten_$lang"] ?>" />
                                            </span>
                                        </a>
                                    </div>
                                    <h2 class="single-product-name"><a href="<?= $tintuc_khac[$j]["tenkhongdau_$lang"] ?>"><?= $tintuc_khac[$j]["ten_$lang"] ?></a></h2>
                                    
                                </div>
                            </div>
                            
                            <?php } ?>
                            
                        </div>
                    </div>
                </div>    
            </div>
            <div class="col-xl-3 col-lg-3">
                <div class="single-products-category">
                    <div class="section-title"><h2>TIN TRONG NGÀNH</h2></div>
                    <div class="category-products">
                        <?php
                            
                                for ($j = 0, $count_spmoi = count($tintuc_khac); $j < 3; $j++) { ?> 
                        
                        <div class="product-items">
                            <div class="p-category-image">
                                <a href="<?= $tintuc_khac[$j]["tenkhongdau_$lang"] ?>">
                                    <img alt="" src="thumb/80x100/1/<?php
                                            if ($tintuc_khac[$j]['photo'] != NULL)
                                                        echo _upload_news_l . $tintuc_khac[$j]['photo'];
                                            else
                                                            echo 'images/no-image-available.png'; ?>"
                                                    alt="<?= $tintuc_khac[$j]["ten_$lang"] ?>" />
                                </a>
                            </div>
                            <div class="p-category-text">
                                <h2 class="category-product-name"><a href="<?= $tintuc_khac[$j]["tenkhongdau_$lang"] ?>"><?= $tintuc_khac[$j]["ten_$lang"] ?></a></h2>
                                
                            </div>
                        </div>
                        <?php } ?>
                        
                        
                        
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="section-title no-margin"><h2>Tags Tìm Kiếm</h2></div>
                    <div class="popular-tags">
                        <ul class="tag-list">                  
                            <li><a href="#">bộ Bé Trai</a></li>
                            <li><a href="#">bộ Bé Gái</a></li>
                            <li><a href="#">bộ mặc nhà</a></li>
                            <li><a href="#">bộ dã ngoại</a></li>
                            <li><a href="#">bộ lót bé gái</a></li>
                            <li><a href="#">bộ lót bé trai</a></li>
                            <li><a href="#">quần lót bé gái</a></li>
                            <li><a href="#">quần lót bé trai</a></li>
                            <li><a href="#">áo thun dây</a></li>
                            <li><a href="#">áo ba lỗ</a></li>
                            <li><a href="#">set mặc nhà</a></li>
                            <li><a href="#">set t-shirt</a></li>
                            <li><a href="#">set ba lỗ</a></li>
                        </ul>
                        <!--   <div class="tag-actions">
                            <a href="#">View All Tags</a>
                        </div> -->
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="section-title no-margin"><h2>ALBUMS MỚI </h2></div>
                    <div class="block-content">
                        <p class="empty">Cập nhật mẫu mới nhất hiện nay</p>
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="banner-box">
                        <a href="#"><img alt="" src="img/banner/14.jpg"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Product Details Area-->