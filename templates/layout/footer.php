<footer class="footer footer-home-three">
	<div class="container">
		<div class="footer-widget-padding">
			<div class="row">
				<div class="col-xl-3 col-lg-3 col-md-6 col-12">
					<div class="single-widget mb-sm-30">
						<div class="footer-widget-title">
							<h3>Về Mykids Store</h3>
						</div>
						<div class="footer-widget-list ">
							<ul>
								<li><a href="#">Giới thiệu Mykids Store</a></li>
								<li><a href="#">Tuyển dụng khối văn phòng</a></li>
								<li><a href="#">Tuyển dụng khối siêu thị</a></li>
								<li><a href="#">Chính sách bảo mật</a></li>
								<li><a href="#">Điều khoản sử dụng</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6 col-12">
					<div class="single-widget mb-sm-30">
						<div class="footer-widget-title">
							<h3>Hỗ trợ khách hàng</h3>
						</div>
						<div class="footer-widget-list ">
							<ul>
								<li><a href="#">Mua & giao nhận online</a></li>
								<li><a href="#">Qui định & hình thức thanh toán</a></li>
								<li><a href="#">Bảo hành & bảo trì</a></li>
								<li><a href="#">Đổi trả & hoàn tiền</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6 col-12">
					<div class="single-widget">
						<div class="footer-widget-title">
							<h3>Đơn vị vận chuyển</h3>
						</div>
						<div class="footer-widget-list ">
							<div class="van-chuyen row">
								<div class="item-logo col-6"><a><img class="img-fluid" src="img/footer/ahamove.png" /></a></div>
								<div class="item-logo col-6"><a><img class="img-fluid" src="img/footer/grab-express.png" /></a></div>
								<div class="item-logo col-6"><a><img class="img-fluid" src="img/footer/viettel.png" /></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6 col-12">
					<div class="single-widget">
						<div class="footer-widget-title">
							<h3>Cách thức thanh toán</h3>
						</div>
						<div class="footer-widget-list ">
							<div class="thanh-toan row">
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/cod.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/atm.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/vnpay.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/visa.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/master.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/jcb.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/zalopay.png" /></a></div>
								<div class="item-logo col-4"><a><img class="img-fluid" src="img/footer/momo.png" /></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="footer-padding">
			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-7 d-flex">
					<div class="footer-logo">
						<a href="index"><img class="img-fluid" src="img/logo/logo.png" alt=""></a>
					</div>
					<div class="footer-widget-list">
						<h2 style="margin-left: 25px;color:#000;">MY KIDS STORE</h2>
						<ul class="address">
							<li><span class="fa fa-phone fa-fw"></span><strong> Điện thoại: </strong><?=$row_setting['hotline']?></li>
							<li><span class="fa fa-envelope-o fa-fw"></span><strong> Email: </strong><?=$row_setting['email']?></li>
							<li><span class="fa fa-map-marker fa-fw"></span><strong> Địa chỉ: </strong><?=$row_setting['diachi_vi']?></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-2">
					<a href="">
						<img src="img/footer/noticed.png" alt="">
					</a>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-3 text-right">
					<div class="footer-widget-title">
						<h3>Kết nối với chúng tôi</h3>
					</div>
					<div class="footer-widget-list float-right">
						<ul class="social-link">
							<li><a target="_blank" href="https://www.facebook.com/mykidsstoreHCM"><i class="icon icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon icon-youtube"></i></a></li>
							<li><a href="#"><i class="icon icon-instagram"></i></a></li>
						</ul>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
	window.fbAsyncInit = function() {
		FB.init({
		xfbml            : true,
		version          : 'v8.0'
		});
	};

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<!-- Your Chat Plugin code -->
<div class="fb-customerchat" attribution=setup_tool page_id="101926398341636"></div>