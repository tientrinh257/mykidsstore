<header class="home-three-header">
    <!-- <div class="header-top-home-three">
        <div class="container">
            <div class="header-container">
                <div class="row">
                    <div class="col-lg-6 col-md-7">
                        <div class="header-contact">
                        <a href="mailto:<?=$row_setting['email']?>"><span class="email">Email: <?=$row_setting['email']?> </span></a> / <a href="tel:<?=$row_setting['hotline']?>"><span class="phone">Phone: <?=$row_setting['hotline']?></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="header-main-home-three">
        <div class="container border-bottom">
            <div class="header-content-home-three">
                <div class="row justify-content-between">
                    <div class="col-lg-4 col-md-8">
                        <div id="search-category">
                            <!-- <form class="search-box-three" action="#" method="post">
                                <input type="search" placeholder="Search here..." name="search">
                                <button id="btn-search-three" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </form> -->
                        </div>
                    </div>
                    <?php include _template."layout/mini-cart.php";?>
                </div>
            </div>
        </div>
    </div>
    <!--Mainmenu Start-->
    <?php include _template."layout/menu.php";?>
    <!-- Mobile Menu Area end -->
 </header>