<?php 
	$d->reset();
	$sql_news = "select ten_$lang,tenkhongdau_$lang,id,photo,thumb,mota_$lang from #_info where hienthi=1 and com='gioithieu'  ";
	$d->query($sql_news);
	$gioithieu_nb = $d->result_array();
	
	$d->reset();
	$sql = "select photo,link, ten_$lang as ten, mota_$lang as mota from #_image_url where hienthi=1 and com='slideabout' order by stt,id desc";
	$d->query($sql);
	$result_hgt = $d->result_array();
	
?>
<div class="box_about">
<div class="container pd0 border_gt" >
        <div class="about_index">  
			<div class="row pd0 mg0">				
				<div class=" col-lg-5 col-md-6 col-sm-12 col-xs-12  ">
					<div class="row pd0 mg0">
						<div class="title_about ">
							<h3><?=$gioithieu_nb[0]["ten_$lang"]?></h3>
						</div>
						<a href="gioi-thieu" >
						<div class="text_gt ">                                            
							<?=catchuoi($gioithieu_nb[0]["mota_$lang"],1000)?>
						</div>
						
						<a href="gioi-thieu" >
							<p  class="icon_xemthem"><?=_xemthem?></p>
						</a>
						
								
					</div>		
				</div>
				
				<div class=" col-lg-7 col-md-6 col-sm-12 col-xs-12 ">
					  <div class="hinhgt slick_gioithieu run_none">
						  <?php for($i=0;$i<count($result_hgt);$i++){ ?>
						  <div class="wap_bao">
							<div class="baohinh"><img src="thumb/440x310/1/<?=_upload_hinhanh_l.$result_hgt[$i]["photo"]?>" alt="<?=$result_hgt[$i]["ten"]?>" /></div>
						  </div>
						  <?php }?>
					  </div>						
											   				
				</div>

			</div>
                  
		</div>
	
</div>

</div>


<script type="text/javascript">
	$(document).ready(function() {
		  $('.slick_gioithieu').slick({
	      slidesToShow: 3,
	      slidesToScroll: 1,
	      
	      dots: false,
	      centerMode: false,
	      infinite: true,
	      autoplay:false,
	      arrows:true,
	      autoplaySpeed:5000,  //Tốc độ chạy
	      focusOnSelect: false,
	      variableWidth:false,
	    });
		
		$('.slick_gioithieu .slick-current').next('.slick-active').addClass('zoom');
		$('.slick_gioithieu').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		  $('.slick_gioithieu .slick-current').next('.slick-active').removeClass('zoom');
		});
		$('.slick_gioithieu').on('afterChange', function(event, slick, currentSlide, nextSlide){
		  $('.slick_gioithieu .slick-current').next('.slick-active').addClass('zoom');
		});
	});
</script>