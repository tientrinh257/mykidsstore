

<?php 

	  
	$d->reset();
	$sql="select ten_$lang,tenkhongdau_$lang,id,photo,mota_$lang,ngaytao,mota_$lang as mota from #_news where hienthi=1 and com='news' and tinnoibat>0 order by stt asc ";
	$d->query($sql);
	$truyenthong=$d->result_array();

?>

<div class="box_tienich">
<div class=" container2">

    <div class="row pd0 mg0">	
		
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
				<div class="title_tienich">
					<h3 >Tin tức sự kiện</h3><span></span>
				</div>
				  <div class="box_tin_nb wow zoomInUp">
                    <div class="row pd0 mg0 box_tin_none">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 pd0">
                            <a href="<?= $truyenthong[0]['tenkhongdau_' . $lang] ?>" title="<?= $truyenthong[0]['ten_' . $lang] ?>">
                                <div class="one_news">
                                    <img src="thumb/320x225/1/<?= _upload_news_l . $truyenthong[0]["photo"] ?>" alt="<?= $truyenthong[0]['ten_' . $lang] ?>" > 
                                    <h3><?= $truyenthong[0]['ten_' . $lang] ?></h3>
								
								
                                    <p><?= catchuoi($truyenthong[0]['mota_' . $lang], 120) ?></p>
									
									
										<p class="icon_xemthem2">Xem thêm</p>
									
                                </div>
                            </a>
							
							
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
						<div id="bx-pagerdv">
                            <?php for ($i = 1; $i < count($truyenthong); $i++) { ?>
							<div>
                                <a href="<?= $truyenthong[$i]['tenkhongdau_' . $lang] ?>" title="<?= $truyenthong[$i]['ten_' . $lang] ?>">
                                    <div class="list_tin_nb">
                                        <img src="thumb/130x100/1/<?= _upload_news_l . $truyenthong[$i]["photo"] ?>" alt="<?= $truyenthong[$i]['ten_' . $lang] ?>" > 
                                        <h3><?= $truyenthong[$i]['ten_' . $lang] ?></h3>

									
                                        <p><?= catchuoi($truyenthong[$i]['mota_' . $lang], 80) ?></p>
										<div class="clear">
										</div>
                                    </div>
                                </a>
								</div>
                            <?php } ?>
							</div>
                        </div>
                    </div>
                    <ul class="list_rp_nb" style="display: none">
                        <?php for ($i = 0; $i < count($truyenthong); $i++) { ?>
                            <li>
                                <a href="tin-tuc/<?= $truyenthong[$i]['tenkhongdau_' . $lang] ?>-<?= $truyenthong[$i]['id'] ?>.html" title="<?= $truyenthong[$i]['ten_' . $lang] ?>"><?= $truyenthong[$i]['ten_' . $lang] ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
		</div>	
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
		<div class="title_tienich">
			<h3 >Video Clip</h3><span></span>
		</div>	
			<?php include _template."layout/video.php";?>
		</div>
		
		
	</div>

</div>
</div> 
<div class="box_visao">
<div class="container">
	<div class="row pd0 mg0 ">
		<div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 ">
		</div>
		<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 ">	
		<div class="title_right_dknt  wow zoomInUp"  >
		<a  >
		<h2 > Đăng ký nhận tin</h2>			
		</a>
		<p>Hãy đăng ký nhận thông tin khuyến mãi mới nhất từ chúng tôi.</p>
		</div>
		<div class="newsletter wow flipInX">
				
                <form action="#" method="post" id="subscribe_form" name="subscribe-form">
				<div class="row pd0 mg0 ">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd0">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5">
					<input type="text" name="first_name" id="first_name" placeholder="<?= _nameError ?>....." onfocus="if (this.value == '<?= _nameError ?>.....')
						this.value = ''" class="form_newsletter">
				</div>
			
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5">	
					<input type="text" name="diachi_newsletter" id="diachi_newsletter" placeholder="<?= _addressError ?>....." onfocus="if (this.value == '<?= _addressError ?>.....')
						this.value = ''" class="form_newsletter">
				</div>	
					
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd0">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5">	
						<input type="email" name="email_newsletter" id="email_newsletter" class="form_newsletter" placeholder="<?= _emailError ?>....." onfocus="if (this.value == '<?= _emailError ?>.....')
							this.value = ''" class="form_newsletter">
					</div>
						
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5">	
						<input type="text" name="phone_newsletter" id="phone_newsletter" placeholder="<?= _phoneError ?>....." onfocus="if (this.value == '<?= _phoneError ?>.....')
							this.value = ''" class="form_newsletter">
					</div>	
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pd0">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd5">
					
						
						<textarea placeholder="<?= _noidung ?>" name="noidung_newsletter" rows="4" col="50" id="noidung_newsletter" class="form_newsletter"></textarea>
				</div>
				</div>
				<div class="col-lg6 col-md-6 col-sm-6 col-xs-6 pd5 ">
                    <input type="submit" value="Liên hệ ngay" name="subscribe-go" id="send_email_newsletter" class="button">
				</div>
				<div class="col-lg6 col-md-6 col-sm-6 col-xs-6 pd5 ">
                   <input type="button" value=" Nhập lại " class="button send_email_newsletter" onclick="document.subscribe-form.reset();">
				</div>	
				</div>	
                </form>

                <div class="clear"></div>

             <script type="text/javascript">
                    $(document).ready(function (e) {
                        $('#send_email_newsletter').click(function () {
							
							var opt = $('#opt');
							
                            var el = $('#email_newsletter');
                            var sex = $('#phone_newsletter');
                            var first_name = $('#first_name');
                            var last_name = $('#last_name');
							var noidung_newsletter = $('#noidung_newsletter');
							
							var diachi_newsletter = $('#diachi_newsletter');
							var tieude_newsletter = $('#tieude_newsletter');
							
                            var emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;
							if (first_name.val() == '') {
                                first_name.focus();
                                alert('<?= _nameError ?>');
                                return false;
                            }
                            if (el.val() == '') {
                                el.focus();
                                alert('<?= _emailError ?>');
                                return false;
                            }
						
                            if (!emailRegExp.test(el.val())) {
                                el.focus();
                                alert('<?= _emailError1 ?>');
                            } else {
								
							grecaptcha.ready(function() {
							grecaptcha.execute('<?=$config_site?>', {action: 'newsletter'}).then(function(token) {
								// add token to form
							$('#subscribe_form').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
								
								$.ajax({
								type: 'POST',
								url: 'ajax/newsletter.php',
								data: 'email=' + el.val() + '&sex=' + sex.val() + '&first_name=' + first_name.val() + '&last_name=' + last_name.val() + '&noidung=' + noidung_newsletter.val() + '&token=' + token+ '&diachi_newsletter=' + diachi_newsletter.val() ,
								success: function (result) {
									if(result==0){
										swal("Vượt quá số lần gửi mail. Vui lòng thực hiện lại sau vài phút!", "", "error");
									}else{
										if(result==1){
											swal("<?=_emailnaydaduocdangky?>!", "", "success");
										}else if(result==2){
											swal("<?=_bandangkythanhcong?>!", "", "success");
										}else if(result==3){
											swal("<?=_vuilongquaylaisau?>!", "", "success");
										}
										
									}
								}
								});
								
							});
							});
								
								
								
								
                               
                            }
                            return false;
                        });
                    });
                </script>


            </div>
		</div>
		<div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 ">
		</div>
		</div>
	
	
	
</div>	
</div>
<script type="text/javascript">
	$(document).ready(function () {
				
		$('#bx-pagerdv').slick({
			
			autoplay:true,
			arrows:false,
			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 1,
			vertical:true,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: true,
						dots: false
					}
				},
				{
					breakpoint: 700,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1
					}
				}

			]
		});

	});
</script>
		
