<?php
    if(@$_REQUEST['command']=='delete' && $_REQUEST['pid']>0){
		remove_product($_REQUEST['pid']);
    }
?>
<div class="col-lg-4 col-md-4 col-10">
    <ul class="header-r-cart header-cart-three">
        <?php $cart_total_item = get_total_item(); ?>
        <li><a class="cart" href="<?= base_url('gio-hang') ?>">My Cart: <?=$cart_total_item ?> items</a>
            <div class="mini-cart-content">
                <div class="cart-products-list">
                    <form name="formminicart" method="post">
                        <input type="hidden" name="pid" />
                        <input type="hidden" name="command" />
                        <?php if( $cart_total_item > 0 ) { ?>
                            <?php foreach ($_SESSION['cart'] as $pid => $value) { 
                                $product_detail = get_product_detail($pid); ?>
                                <div class="cart-products">
                                    <div class="cart-image">
                                        <a href="<?= $product_detail["tenkhongdau_$lang"] ?>"><img alt="<?= $product_detail['tenkhongdau_vi'] ?>" src="<?= 'thumb/75x83/1/' ._upload_product_l . $product_detail['photo'] ?>" alt=""></a>
                                    </div>
                                    <div class="cart-product-info">
                                        <a href="<?= base_url($product_detail["tenkhongdau_$lang"]) ?>" class="product-name"> <?= $product_detail["tenkhongdau_$lang"] ?> </a>
                                        <a href="<?= base_url('gio-hang') ?>" class="edit-product">Sửa</a>
                                        <a href="javascript:del(<?=$pid?>)" class="remove-product">Xóa</a>
                                        <div class="price-times">
                                            <span class="quantity"><strong> <?= $value['qty'] ?> x</strong></span>
                                            <span class="p-price"><?= number_format($value["price"],0,',','.') ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?> 
                        <?php } else { ?>
                            <?= _emptycart ?>
                        <?php }?>
                    </form>
                </div>
                <div class="cart-price-list">
                    <p class="price-amount">Tổng : <span><?= number_format(get_order_total(),0, ',', '.') ?> vnđ</span></p>
                    <div class="cart-buttons">
                        <a href="<?= base_url('thanh-toan') ?>">Thanh toán </a>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<script type="text/javascript">
    function del(pid){
        if(confirm('Bạn có thực sự muốn xóa mục này')){
            document.formminicart.pid.value=pid;
            document.formminicart.command.value='delete';
            document.formminicart.submit();
        }
    }
</script>