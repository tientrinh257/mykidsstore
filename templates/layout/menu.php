<div class="mainmenu-area-home-three d-lg-block d-none">
    <div id="sticker">
        <div class="container">
            <div class="row">
                <div class="col-md-1 no-padding">
                    <div class="logo" style="max-width:100%">
                        <a href="<?= base_url() ?>"><img class="img-fluid" src="img/logo/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-md-11 d-xl-block d-lg-block d-none position-relative">
                    <div class="mainmenu position-absolute">
                        <nav>
                            <ul id="nav">
                                <li class="current"><a href="<?= base_url(); ?>">Trang Chủ</a>
                                    <ul class="sub-menu">
                                        <li><a href="gioi-thieu">Giới Thiệu</a></li>
                                        <li><a href="chinh-sach-uu-dai">Chính Sách Ưu Đãi</a></li>
                                        <li><a href="chinh-sach-dai-ly">Chính Sách Đại Lý</a></li>
                                        <li><a href="chinh-sach-van-chuyen">Chính Sách Vận Chuyển</a></li>
                                    </ul>
                                </li>
                                <?php 
                                    $d->query("select ten_$lang,tenkhongdau_$lang,id from #_product_list where hienthi=1 order by stt");
                                    $list=$d->result_array();
                                ?>
                                <?php for($i=0;$i<count($list);$i++) { ?>
                                    <li><a href="<?=base_url().$list[$i]['tenkhongdau_vi']?>" title="<?=$list[$i]['ten_'.$lang]?>"><?=$list[$i]['ten_'.$lang]?></a>
                                        <?php $d->query("select ten_$lang,tenkhongdau_$lang,id from #_product_cat where hienthi=1 and id_list='".$list[$i]['id']."' order by stt asc");
                                            $cap2=$d->result_array();
                                            if( count($cap2) > 0) {
                                        ?>
                                        <div class="megamenu">
                                            <div class="megamenu-list clearfix">
                                                <span>
                                                    <?php for($k=0;$k<count($cap2);$k++){?>
                                                        <a href="<?= base_url().$cap2[$k]['tenkhongdau_vi']?>" title="<?=$cap2[$k]['ten_'.$lang]?>"><?=$cap2[$k]['ten_'.$lang]?></a>
                                                    <?php }?>
                                                </span>
                                                <span class="no-margin">
                                                    <a href="<?= base_url().$list[$i]['tenkhongdau_vi'] ?>" class="mega-banner">
                                                        <img src="img/mega-banner.jpg" alt="">
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                <?php } }?>
                                <li><a href="tin-tuc">Tin Tức</a></li>
                                <li><a href="lien-he">Liên Hệ</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End of Mainmenu-->
<!-- Mobile Menu Area start -->
<div class="mobile-menu-area fix">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul>
                            <li class="current"><a href="index">Trang Chủ</a>
                                <ul class="sub-menu">
                                    <li><a href="gioi-thieu">Giới Thiệu</a></li>
                                    <li><a href="chinh-sach-uu-dai">Chính Sách Ưu Đãi</a></li>
                                    <li><a href="chinh-sach-dai-ly">Chính Sách Đại Lý</a></li>
                                    <li><a href="chinh-sach-van-chuyen">Chính Sách Vận Chuyển</a></li>
                                </ul>
                            </li>
                            <?php 
                                $d->query("select ten_$lang,tenkhongdau_$lang,id from #_product_list where hienthi=1");
                                $list=$d->result_array();
                            ?>
                            <?php for($i=0;$i<count($list);$i++){?>
                                <li><a href="<?=$list[$i]['tenkhongdau_vi']?>" title="<?=$list[$i]['ten_'.$lang]?>"><?=$list[$i]['ten_'.$lang]?></a>
                                    <!-- <div class="megamenu">
                                        <div class="megamenu-list clearfix">
                                            <span>
                                                <?php 
                                                    $d->query("select ten_$lang,tenkhongdau_$lang,id from #_product_cat where hienthi=1 and id_list='".$list[$i]['id']."' order by stt asc");
                                                    $cap2=$d->result_array();
                                                ?>
                                                <?php for($k=0;$k<count($cap2);$k++){?>
                                                    <a href="<?=$cap2[$k]['tenkhongdau_vi']?>" title="<?=$cap2[$k]['ten_'.$lang]?>"><?=$cap2[$k]['ten_'.$lang]?></a>
                                                <?php }?>
                                            </span>
                                            <span class="no-margin">
                                                <a href="#" class="mega-banner">
                                                    <img src="img/mega-banner.jpg" alt="">
                                                </a>
                                            </span>
                                        </div>
                                    </div> -->
                                </li>
                            <?php }?>
                            <li><a href="lien-he">LIÊN HỆ</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>