<base href="<?= base_url() ?>/" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php
    if (isset($title_bar))
        echo $title_bar;
    else
        echo $row_setting["title_$lang"];
    ?>
</title>
<meta charset="utf-8">
<!-- <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<meta name="robots" content="index, follow" />
<meta name="author" content="<?= $row_setting["author_web"] ?>">
<meta name="keywords" content="<?= $row_setting["keywords_$lang"] ?>" />
<meta name="description" content="<?= $row_setting["description_$lang"] ?>" />
<meta http-equiv="Content-Language" content="vi" />
<meta name="Language" content="vietnamese" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?= $title_bar ?>" />
<meta property="og:image" content="<?= $image ?>" />
<meta property="article:publisher" content="<?= $row_setting["fanpage"] ?>" />
<meta property="og:site_name" content="<?= $row_setting["ten_$lang"] ?>"/>
<meta property="og:url" content="<?= $url_web ?>" />
<meta property="og:description" content="<?= $description_web ?>" />
<meta property="dcterms:title" content="<?= $title_bar ?>" />
<meta property="dcterms:identifier" content="<?= $url_web ?>" />
<meta property="dcterms:description" content="<?= $description_web ?>" />
<meta property="dcterms:subject" content="<?= $row_setting["keywords_$lang"] ?>" /> -->

<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="website">
<meta property="og:title" content="MyKidsStore" />
<meta property="og:url" content="https://mykidsstore.vn/">
<meta property="og:site_name" content="MyKidsStore">
<meta property="og:image" content="https://mykidsstore.vn/img/logo/logo.png">
<meta property="og:description" content="<?= $row_setting["description_$lang"] ?>">
<meta name="description" content="<?= $row_setting["description_$lang"] ?>">
<meta name="keywords" content="<?= $row_setting["keywords_$lang"] ?>">
<meta name="robots" content="index, follow">
<meta name="author" content="mykidsstrore">
<?php if (isset($com)) {
  if( $com == "index.php" || $com = "index" || $com =""){ ?>
  <link rel="amphtml" href="<?= base_url('amp.php') ?>">
<?php } } ?>
<link rel="canonical" href="<?= getCurrentPageURL() ?>" >
<title>MyKidsStore</title>

<!-- Dublin Core -->
<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
<meta name="DC.title" content="<?= $title_bar ?>">
<meta name="DC.identifier" content="<?= $url_web ?>">
<meta name="DC.description" content="<?= $description_web ?>"/>
<meta name="DC.subject" content="<?= $row_setting["keywords_$lang"] ?>">
<meta name="DC.language" scheme="UTF-8" content="vi,en">

<meta itemprop="name" content="<?= $row_setting["title_$lang"] ?>">
<meta property="twitter:title" content="<?= $title_bar ?>"/>
<meta property="twitter:url" content="<?= $url_web ?>"/>
<meta property="twitter:card" content="summary"/>    

<meta name="twitter:description" content="<?= $description_web ?>"/>
<meta name="twitter:image" content="<?= $image ?>" />
<meta property="article:author" content="<?= $url_web ?>" />
<?= $row_setting["geo_meta"]; ?>

<!-- Thêm cho Google Plus -->
<meta name="author" content="mykidsstore" />
<meta name="copyright" content="mykidsstore" />
<meta name="robots" content="index,follow" />
<meta name='revisit-after' content='1 days' />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "MyKidsStore",
  "image": "https://mykidsstore.vn/img/logo/logo.png",
  "@id": "",
  "url": "https://mykidsstore.vn/",
  "telephone": "0788733999",
  "priceRange": "0",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "1800 Nguyễn Duy Trinh, Phường Trường Thành, Quận 9",
    "addressLocality": "Hồ Chí Minh",
    "postalCode": "700000",
    "addressCountry": "VN"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 10.8078134,
    "longitude": 106.7784724
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "00:00",
    "closes": "23:59"
  },
  "sameAs": "https://www.facebook.com/mykidsstoreHCM"
}
</script>

<?= $row_setting["code_analytics"]; ?>
<h1 style="display:none;"><?= $row_setting["h1_$lang"]; ?></h1>
<h2 style="display:none;"><?= $row_setting["h2_$lang"]; ?></h2>
<!-- favicon
============================================ -->		
<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

<!-- Google Fonts
============================================ -->		
<link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,900,700,700italic,300' rel='stylesheet' type='text/css'>

<!-- Bootstrap CSS
============================================ -->		
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- nivo slider CSS
============================================ -->
<link rel="stylesheet" href="lib/nivo-slider/css/nivo-slider.css" type="text/css" />
<link rel="stylesheet" href="lib/nivo-slider/css/preview.css" type="text/css" media="screen" />

<!-- Fontawsome CSS
============================================ -->
<link rel="stylesheet" href="css/font-awesome.min.css">

<!-- owl.carousel CSS
============================================ -->
<link rel="stylesheet" href="css/owl.carousel.css">

<!-- jquery-ui CSS
============================================ -->
<link rel="stylesheet" href="css/jquery-ui.css">

<!-- meanmenu CSS
============================================ -->
<link rel="stylesheet" href="css/meanmenu.min.css">

<!-- animate CSS
============================================ -->
<link rel="stylesheet" href="css/animate.css">

<!-- style CSS
============================================ -->
<link rel="stylesheet" href="style.css">

<!-- responsive CSS
============================================ -->
<link rel="stylesheet" href="css/responsive.css">

<!-- modernizr JS
============================================ -->		
<script src="js/vendor/modernizr-2.8.3.min.js"></script>
<!-- jquery
============================================ -->		
<script src="js/vendor/jquery-1.11.3.min.js"></script>