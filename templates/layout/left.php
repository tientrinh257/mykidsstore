<?php
$d->reset();
$sql = "select ten_$lang as ten, tenkhongdau_$lang as tenkhongdau, id from #_product_list where hienthi=1 and com='product' order by stt,id desc";
$d->query($sql);
$product_cap1 = $d->result_array();

$d->reset();
$sql = "select ten_$lang as ten,tenkhongdau_$lang as tenkhongdau,id from #_news_list where hienthi=1  and com='dichvu' order by stt asc";
$d->query($sql);
$dv_list = $d->result_array();

$d->reset();
$sql_hotline = "select * from #_support_online where com='support_online' ";
$d->query($sql_hotline);
$row_hotline = $d->result_array();

$d->reset();
$sql = "select ten_$lang,id,link,photo from #_image_url where hienthi=1 and com='mangxahoi' order by stt asc";
$d->query($sql);
$mxh = $d->result_array();

$d->reset();
$sql = "select ten_$lang,id,link from #_image_url where hienthi=1 and com='linkmenu' order by stt asc";
$d->query($sql);
$lkwebitem = $d->result_array();

$d->reset();
$sql_lienket = "select ten_$lang as ten,yahoo,skype,dienthoai,email from #_support_online where hienthi=1 and com='support_online' order by id desc ";
$d->query($sql_lienket);
$support_online = $d->result_array();

?>



<style type="text/css">

    ul.block_ul
    {
        display: block !important; 
    }
</style>
<div class="dm_left col-lg-12 col-md-12 col-sm-4 col-xs-12 pd0">
	 <?php
            $d->reset();
            $sql = "select ten_$lang as ten, tenkhongdau_$lang as tenkhongdau, id,hienthitc from #_product_list where hienthi=1  order by stt,id desc";        
            $d->query($sql);
            $product_cap2 = $d->result_array();
        
            
    ?>
	<div class="cate-pro">
	  
		<h4 class="title-catalog">Danh mục sản phẩm</h4>
	</div><!--cate-pro-->	
	<div id="nav">
        <ul id="accordion">
			<?php for ($i = 0, $count_dmsp1 = count($product_cap2); $i < $count_dmsp1; $i++) {?>
			<li><a href="<?= $product_cap2[$i]['tenkhongdau'] ?>" title="<?= htmlentities($product_cap2[$i]['ten'], ENT_QUOTES, "UTF-8") ?>"><?= $product_cap2[$i]['ten'] ?><?php if($product_cap2[$i]['hienthitc'] >0){ ?>    <?php }?></a>
				<ul>
				<?php
							$d->reset();
							$sql = "select ten_$lang as ten, tenkhongdau_$lang as tenkhongdau, id from #_product_cat where hienthi=1 and id_list='".$product_cap2[$i]["id"]."' order by stt,id desc";
							$d->query($sql);
							$project_cap3 = $d->result_array();
							
							?>
						<?php if(count($project_cap3)>0) {?>
						 
							<?php for ($j = 0, $count_cat = count($project_cap3); $j < $count_cat; $j++) { ?>
								<li><a href="<?= $project_cap3[$j]['tenkhongdau'] ?>" title="<?= htmlentities($project_cap3[$j]['ten'], ENT_QUOTES, "UTF-8") ?>"><?= $project_cap3[$j]['ten'] ?></a>
									<ul>
									<?php
										$d->reset();
										$sql = "select ten_$lang as ten, tenkhongdau_$lang as tenkhongdau, id from #_product_item where hienthi=1 and id_cat='".$project_cap3[$j]["id"]."' order by stt,id desc";
										$d->query($sql);
										$project_cap4 = $d->result_array();
										?>
									<?php if(count($project_cap4)>0) {?>
									 
										<?php for ($z = 0, $count_item = count($project_cap4); $z < $count_item; $z++) { ?>
											<li><a href="<?= $project_cap4[$z]['tenkhongdau'] ?>" title="<?= htmlentities($project_cap4[$z]['ten'], ENT_QUOTES, "UTF-8") ?>"><?= $project_cap4[$z]['ten'] ?></a></li>
										<?php } ?>
								 
									<?php }?>
									</ul>
								</li>
							<?php } ?>
					 
				<?php }?>
				</ul>
			</li>			
			<?php }?>		
        </ul>
    </div> 
</div>

<div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 pd0">
	<div class="cate-pro">
		<h4 class="title-catalog">Hotline hỗ trợ</h4>
	</div>
    <div class="sub_con_other">
        <div class="hotline_sup">
            <img src="images/image_hotro.png" />
            <div class="right_hotsup">             
                <p> <?= $row_setting["hotline"] ?></p>
				<p> <?= $row_setting["dienthoai"] ?></p>
            </div>
        </div>
        <div class="clear"></div>

        <?php for ($i = 0; $i < count($support_online); $i++) { ?>   
            <div class="info_support">
                <div class="name_skype_yahoo">
                   <p><?= $support_online[$i]["ten"] ?> <a class="yahoo"  href="<?= $support_online[$i]["skype"] ?>"><img src="images/icon_skype.png"></a><a class="yahoo"  href="https://zalo.me/<?= $support_online[$i]["yahoo"] ?>"><img src="images/icon_zaloht.png"></a></p>
                </div>
		
                <p class="sup_phone">Hotline:<span><?= $support_online[$i]["dienthoai"] ?></span></p>
				<p class="sup_email">Email:<?= $support_online[$i]["email"] ?></p>
            </div>
        <?php } ?>   
    </div>
</div>	
