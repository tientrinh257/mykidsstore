<!-- popper JS
============================================ -->		
<script src="js/popper.min.js"></script>

<!-- bootstrap JS
============================================ -->		
<script src="js/bootstrap.min.js"></script>

<!-- nivo slider js
============================================ -->       
<script src="lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
<script src="lib/nivo-slider/home.js" type="text/javascript"></script>

<!-- wow JS
============================================ -->		
<script src="js/wow.min.js"></script>
    
<!-- meanmenu JS
============================================ -->		
<script src="js/jquery.meanmenu.js"></script>

<!-- owl.carousel JS
============================================ -->		
<script src="js/owl.carousel.min.js"></script>

<!-- price-slider JS
============================================ -->		
<script src="js/jquery-price-slider.js"></script>	

<!-- scrollUp JS
============================================ -->		
<script src="js/jquery.scrollUp.min.js"></script>

<!--Countdown js-->
<script src="js/jquery.countdown.min.js"></script>

<!-- Sticky Menu JS
============================================ -->		
<script src="js/jquery.sticky.js"></script>

<!-- Elevatezoom JS
============================================ -->		
<script src="js/jquery.elevateZoom-3.0.8.min.js"></script>

<!-- plugins JS
============================================ -->		
<script src="js/plugins.js"></script>

<!-- main JS
============================================ -->		
<script src="js/main.js"></script>
<script src="js/cart.js"></script>
<a id="phone-icon" href="tel:<?=$row_setting['hotline']?>"><i class="fa fa-phone fa-fw"></i></a>