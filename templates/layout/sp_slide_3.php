<script type="text/javascript">
    $(window).load(function () {
		callback_slick_bv();	
     
    });
	
	 function callback_slick_bv() {
				 $('.box-line-bv').slick({
					autoplay:false,
                    dots: false,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 4,
                    slidesToScroll: 1,
				
                    arrows:true,
                    responsive: [
                        {
                            breakpoint: 1124,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                                infinite: true
                               
                            }
                        },
                        {
                            breakpoint: 800,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1
                            }
                        }

                    ]
                });
		}
</script>

<div class="">
    <div class="container" style="padding: 20px 0px">
    
        <div class="content-container news-content">
            <div class="wrap-tabs ">
			<div class="row pd0 mg0 ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd0">
					<div class="title-index wow zoomInUp"  >
						<a href="san-pham" ><h3 >Bảng báo giá</h3></a>
					</div>
				</div>
			</div>
			
                <div class="contenttab" >
                
                        <div class="tab" id="tcontent">
                            <div class="box-line-sp run_none" >
							<?php 
							$d->reset();
							$sql = "select ten_$lang as ten,tenkhongdau_$lang as tenkhongdau,id,photo,thumb from #_news where hienthi=1 and com='khuyenmai' and tinnoibat>0  order by stt,id asc";		
							$d->query($sql);
							$bbg = $d->result_array();	
							for($j=0;$j<count($bbg);$j++) {?>
								<div class="">
									 <div class="item_product wow flipInX" >

											<div class="zoom_product">
												<img src="thumb/270x210/1/<?php
												if ($product_spec[$j]['photo'] != NULL)
													echo _upload_news_l . $bbg[$j]['photo'];
												else
													echo 'images/no-image-available.png';
												?>" alt="<?= $bbg[$j]['ten'] ?>" />
												
												
											</div>
											<div class="name_product">
														<h3>
															<?= $bbg[$j]['ten'] ?>
														</h3>
																									
												</div>
									  
									</div><!--item_product-->
								</div>						
							<?php }?>							
							</div>
                        </div>
	
                    
                </div>
            </div><!-- end wrap-tabs -->

        </div> 
    </div>   
</div>


	