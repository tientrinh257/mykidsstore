<div class="home-three-wrapper">
    <!--End of Banner Area-->
    <!--Product Area Start-->
    <div class="product-area-home-three">
        <div class="container">
            <div class="section-top-padding"> 
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title"><h2><?=$title_view?></h2></div>
                    </div>
                </div>
                <div class="product row">
                    <?php
                        $a_sid=array();
                        if( !empty($_SESSION['ss'])) {
                            foreach($_SESSION['ss'] as $k=>$v){
                                $a_sid[$k]=$v['ssid'];
                            }
                        }
                        for ($j = 0, $count_spmoi = count($product_category); $j < $count_spmoi; $j++) {
                    ?> 
                    
                    <div class="col-xl-2 col-lg-3 col-md-4 col-12">
                        <div class="single-product-item">
                            <div class="sale-product-label"><span>new</span></div>
                            <div class="single-product clearfix">
                                <a href="<?= base_url() .$product_category[$j]["tenkhongdau_$lang"] ?>">
                                    <span class="product-image">
                                        <img src="<?= base_url() ?>thumb/540x728/1/<?php
                                        if ($product_category[$j]['photo'] != NULL)
                                                    echo _upload_product_l . $product_category[$j]['photo'];
                                            else
                                                echo 'images/no-image-available.png'; ?>"
                                                alt="<?= $product_category[$j]["ten_$lang"] ?>" />
                                    </span>
                                </a>
                            </div>
                            <h2 class="single-product-name"><a href="<?= base_url().$product_category[$j]["ten_$lang"] ?>"><?= $product_category[$j]["ten_$lang"] ?></a></h2>
                            <div class="price-box">
                                <p class="old-price">
                                    <span class="price"><?= number_format($product_category[$j]['gia_vnd'],0,",",".")?></span> vnđ
                                </p>
                                <p class="special-price" style="color:#f36e25">
                                    <span class="price" ><?= number_format(get_price($product_category[$j]['id']),0,",",".") ?></span> vnđ
                                </p>
                            </div>
                        </div>
                    </div>
                        <?php } ?>
                    
                </div>
            </div>      
        </div>
    </div>
    <!--End of Product Area-->
</div>