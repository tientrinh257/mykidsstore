<!doctype html>
<html class="no-js" lang="">
    
<!-- Mirrored from demo.hasthemes.com/mozar-preview/mozar/product-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Jan 2020 03:41:38 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Product Details || Mozar</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
        <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,900,700,700italic,300' rel='stylesheet' type='text/css'>
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        <!-- nivo slider CSS
		============================================ -->
		<link rel="stylesheet" href="lib/nivo-slider/css/nivo-slider.css" type="text/css" />
		<link rel="stylesheet" href="lib/nivo-slider/css/preview.css" type="text/css" media="screen" />
        
		<!-- Fontawsome CSS
		============================================ -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
        
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        
		<!-- jquery-ui CSS
		============================================ -->
        <link rel="stylesheet" href="css/jquery-ui.css">
        
		<!-- meanmenu CSS
		============================================ -->
        <link rel="stylesheet" href="css/meanmenu.min.css">
        
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="css/animate.css">
        
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="style.css">
        
		<!-- responsive CSS
		============================================ -->
        <link rel="stylesheet" href="css/responsive.css">
        
		<!-- modernizr JS
		============================================ -->		
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <!--Header Start-->
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="header-container">
                        <div class="row">
                            <div class="col-lg-6 col-md-7">
                                <div class="header-contact">
                                    <span class="email">Email: admin@bootexperts.com </span> / <span class="phone">Phone: +234.546.657</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-5">
                                <div class="currency-language">
                                    <div class="currency-menu">
                                        <ul>
                                            <li><a href="#">USD <i class="fa fa-angle-down"></i></a>
                                                <ul class="currency-dropdown">
                                                    <li><a href="#">EUR</a></li>
                                                    <li><a href="#">USD</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="language-menu">
                                        <ul>
                                            <li><a href="#">English <i class="fa fa-angle-down"></i></a>
                                                <ul class="language-dropdown">
                                                    <li><a href="#">French</a></li>
                                                    <li><a href="#">English</a></li>
                                                    <li><a href="#">German</a></li>
                                                    <li><a href="#">Arabic</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="account-menu">
                                        <ul>
                                            <li><a href="account.html">My Account <i class="fa fa-angle-down"></i></a>
                                                <ul class="account-dropdown">
                                                    <li><a href="wishlist.html">My Wishlist</a></li>
                                                    <li><a href="cart.html">My Cart</a></li>
                                                    <li><a href="checkout.html">Checkout</a></li>
                                                    <li><a href="blog.html">Blog</a></li>
                                                    <li><a href="account.html">Log In</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <div class="header-main">
                <div class="container">
                    <div class="header-content">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12">
                                <div class="logo">
                                    <a href="index.html"><img src="img/logo/logo.png" alt="MOZAR"></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-8">
                                <div id="search-category">
                                    <form class="search-box" action="#" method="post">
                                        <div class="search-categories">
                                            <div class="search-cat">
                                                <select class="category-items " name="category">
                                                    <option>All Categories</option>
                                                    <option>Women</option>
                                                    <option>Men</option>
                                                    <option>Jewllery</option>
                                                    <option>Bootees Bags</option>
                                                    <option>Clothing</option>
                                                    <option>Footwear</option>
                                                    <option>T-Shirts</option>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="search" placeholder="Search here..." id="text-search" name="search">
                                        <button id="btn-search-category" type="submit">
                                            <i class="icon-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-4">
                                <ul class="header-r-cart">
                                    <li><a class="cart" href="cart.html">My Cart:2 items</a>
                                        <div class="mini-cart-content">
                                            <div class="cart-products-list">
                                                <div class="cart-products">
                                                    <div class="cart-image">
                                                        <a href="#"><img src="img/cart/1.jpg" alt=""></a>
                                                    </div>
                                                    <div class="cart-product-info">
                                                        <a href="#" class="product-name"> Donec ac tempus </a>
                                                        <a class="edit-product">Edit item</a>
                                                        <a class="remove-product">Remove item</a>
                                                        <div class="price-times">
                                                            <span class="quantity"><strong> 1 x</strong></span>
                                                            <span class="p-price">$100.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cart-products">
                                                    <div class="cart-image">
                                                        <a href="#"><img src="img/cart/2.jpg" alt=""></a>
                                                    </div>
                                                    <div class="cart-product-info">
                                                        <a href="#" class="product-name">Quisque in arcu </a>
                                                        <a class="edit-product">Edit item</a>
                                                        <a class="remove-product">Remove item</a>
                                                        <div class="price-times">
                                                            <span class="quantity"><strong> 1 x</strong></span>
                                                            <span class="p-price">$260.00</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cart-price-list">
                                                <p class="price-amount">SUBTotal : <span>$356.15</span> </p>
                                                <div class="cart-buttons">
                                                    <a href="checkout.html">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <!--Mainmenu Start-->
            <div class="mainmenu-area d-lg-block d-none">
                <div id="sticker"> 
                    <div class="container">
                        <div class="row">   
                            <div class="col-xl-12 col-lg-12 d-xl-block d-lg-block d-none">
                                <div class="mainmenu">
                                    <nav>
                                        <ul id="nav">
                                            <li class="current"><a href="index.html">Home</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#" class="mega-title">Homepage</a></li>
                                                    <li><a href="index-2.html">Homepage Version 2</a></li>
                                                    <li><a href="index-3.html">Homepage Version 3</a></li>
                                                    <li><a href="index-4.html">Homepage Version 4</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="shop-grid.html">Women</a>
                                                <div class="megamenu">
                                                    <div class="megamenu-list clearfix">
                                                        <span>											
                                                            <a href="#" class="mega-title">Clothing</a>
                                                            <a href="#">Blazers</a>
                                                            <a href="#">Jackets</a>
                                                            <a href="#">Collections</a>
                                                            <a href="#">T-Shirts</a>
                                                        </span>
                                                        <span>											
                                                            <a href="#" class="mega-title">Dresses</a>
                                                            <a href="#">Cocktail</a>
                                                            <a href="#">Sunglass</a>
                                                            <a href="#">Evening</a>
                                                            <a href="#">Footwear</a>
                                                        </span>
                                                        <span>											
                                                            <a href="#" class="mega-title">Handbags</a>
                                                            <a href="#">Bootees Bags</a>
                                                            <a href="#">Furniture</a>
                                                            <a href="#">Exclusive</a>
                                                            <a href="#">Jackets</a>
                                                        </span>
                                                        <span class="no-margin">										
                                                            <a href="#" class="mega-title">Jewellery</a>
                                                            <a href="#">Earrings</a>
                                                            <a href="#">Nosepins</a>
                                                            <a href="#">Braclets</a>
                                                            <a href="#">Bangels</a>
                                                        </span>
                                                        <span class="no-margin">										
                                                            <a href="#" class="mega-banner">
                                                                <img src="img/mega-banner.jpg" alt="">
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><a href="shop-grid.html">Men</a>
                                                <div class="megamenu">
                                                    <div class="megamenu-list clearfix">
                                                        <span>											
                                                            <a href="#" class="mega-title">Clothing</a>
                                                            <a href="#">Blazers</a>
                                                            <a href="#">Jackets</a>
                                                            <a href="#">Collections</a>
                                                            <a href="#">T-Shirts</a>
                                                        </span>
                                                        <span>											
                                                            <a href="#" class="mega-title">Dresses</a>
                                                            <a href="#">Cocktail</a>
                                                            <a href="#">Sunglass</a>
                                                            <a href="#">Evening</a>
                                                            <a href="#">Footwear</a>
                                                        </span>
                                                        <span>											
                                                            <a href="#" class="mega-title">Handbags</a>
                                                            <a href="#">Bootees Bags</a>
                                                            <a href="#">Furniture</a>
                                                            <a href="#">Exclusive</a>
                                                            <a href="#">Jackets</a>
                                                        </span>
                                                        <span class="no-margin">										
                                                            <a href="#" class="mega-title">Jewellery</a>
                                                            <a href="#">Earrings</a>
                                                            <a href="#">Nosepins</a>
                                                            <a href="#">Braclets</a>
                                                            <a href="#">Bangels</a>
                                                        </span>
                                                        <span class="no-margin">										
                                                            <a href="#" class="mega-banner">
                                                                <img src="img/mega-banner.jpg" alt="">
                                                            </a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </li>
                                            <li><a href="shop-grid.html">Footwear</a></li>
                                            <li><a href="index.html">Pages</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#" class="mega-title">Other Pages</a></li>
                                                    <li><a href="about.html">About Us</a></li>
                                                    <li><a href="blog.html">Blog Page</a></li>
                                                    <li><a href="blog-details.html">Blog Details Page</a></li>
                                                    <li><a href="account.html">My Account</a></li>
                                                    <li><a href="cart.html">Cart Page</a></li>
                                                    <li><a href="checkout.html">Checkout Page</a></li>
                                                    <li><a href="product-details.html">Product Details</a></li>
                                                    <li><a href="shop-grid.html">Shop Grid</a></li>
                                                    <li><a href="shop-list.html">Shop List</a></li>
                                                    <li><a href="wishlist.html">Wishlist</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact Us</a></li>
                                        </ul>
                                    </nav>
                                </div>        
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
            <!--End of Mainmenu-->
            <!-- Mobile Menu Area start -->
            <div class="mobile-menu-area fix">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li><a href="index.html">HOME</a>
                                            <ul>
                                                <li><a href="index-2.html">Home 2</a></li>
                                                <li><a href="index-3.html">Home 3</a></li>
                                                <li><a href="index-4.html">Home 4</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">SHOP</a>
                                            <ul>
                                                <li><a href="shop-grid.html">Shop Grid</a></li>
                                                <li><a href="shop-list.html">Shop List</a></li>
                                                <li><a href="product-details.html">Product Details</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="blog.html">Blog</a>
                                            <ul>
                                                <li><a href="blog-details.html">Blog Details</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="about.html">ABOUT</a>
                                        <li><a href="#">PAGES</a>
                                            <ul>
                                                <li><a href="account.html">My Account</a></li>
                                                <li><a href="cart.html">Cart</a></li>
                                                <li><a href="checkout.html">Checkout</a></li>
                                                <li><a href="wishlist.html">Wishlist</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.html">CONTACT</a></li>
                                    </ul>
                                </nav>
                            </div>					
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area end -->
        </header>
        <!--End of Header Area-->
        <!--Breadcrumb Start-->
        <div class="breadcrumb-container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="index">Sản Phẩm</a><span>/ </span></li>
                                <li><strong>Chi Tiết</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End of Breadcrumb-->
        <!--Product Details Area Start-->
        <div class="product-deails-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-9">
                        <div class="product-details-content row">
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="zoomWrapper">
                                    <div id="img-1" class="zoomWrapper single-zoom">
                                        <a href="#">
                                            <img id="zoom1" src="thumb/540x728/1/<?=_upload_product_l.$row_detail['photo']?>" data-zoom-image="thumb/540x728/1/<?=_upload_product_l.$row_detail['photo']?>" alt="big-1">
                                        </a>
                                    </div>
                                    <div class="product-thumb row">
                                        <ul class="p-details-slider" id="gallery_01">
                                            <?php
		                                        	if(count($album_hinh)>0){
			                                    	foreach ($album_hinh as $key => $value) { ?>
                                            <li class="col-lg-12">
                                                <a class="elevatezoom-gallery" href="#" data-image="<?=_upload_product_l.$value['photo']?>" data-zoom-image="thumb/540x728/1/<?=_upload_product_l.$value['photo']?>"><img src="<?=_upload_product_l.$value['photo']?>" alt=""></a>
                                            </li>
                                            <? }
	                                		}
		                                    	?>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="product-details-conetnt">
                                    <div class="shipping">
                                        <div class="single-service">
                                            <span class="fa fa-truck"></span>
                                            <div class="service-text-container">
                                                <h3>GIAO HÀNG MIỄN PHÍ</h3>
                                                <p>Đơn hàng trên 1 triệu</p>
                                            </div>   
                                        </div>
                                        <div class="single-service">
                                            <span class="fa fa-cube"></span>
                                            <div class="service-text-container">
                                                <h3>Gói Quà Miễn Phí</h3>
                                                <p>Đơn hàng trên 500K</p>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="product-name">
                                        <h1> <?=$row_detail['ten_'.$lang]?> </h1>
                                    </div>
                                    <p class="no-rating no-margin"><a href="#">Bạn là người xem thứ 239</a></p>
                                    <p class="availability">Tình Trạng: <span>Hàng Mới</span></p>
                                    <div class="price-box">
                                        <p class="old-price">
                                            <span class="price"><?=$row_detail['gia_vnd']?></span> vnđ
                                        </p>
                                        <p class="special-price">
                                            <span class="price"><?=$row_detail['giamgia']?></span> vnđ
                                        </p> 											
                                    </div>
                                    <div class="details-description">
                                        <p><?=$row_detail['mota_'.$lang]?> 
                                        </p>
                                        
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12">
                                <div class="p-details-tab">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <li><a class="active" data-toggle="tab" role="tab" aria-controls="description" href="#description">Mô Tả Sản Phẩm</a></li>
                                       
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                                <div class="tab-content review product-details">
                                    <div id="description" class="tab-pane show active" role="tabpanel">
                                        <p><?=$row_detail['mota_'.$lang]?> </p>
                                    </div>
                                    <div id="reviews" class="tab-pane" role="tabpanel">
                                        <div class="row">
                                            <div class="col-xl-5 col-lg-3 col-md-12">
                                                <div class="review-left">
                                                    <p><span> Review by </span> <a href="#">BootExperts</a></p>
                                                    <div class="review-rating">
                                                        <p>price</p>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                    </div>
                                                    <div class="review-rating">
                                                        <p>value</p>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                    </div>
                                                    <div class="review-rating">
                                                        <p>quality</p>
                                                        <div class="rating">
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                            <i class="fa fa-star"></i>
                                                        </div>
                                                    </div>
                                                    <p>BootExperts <span class="italic">(Posted on 11/3/2016)</span></p>
                                                </div>
                                            </div>
                                            <div class="col-xl-7 col-lg-9 col-md-12">
                                                <div class="review-right">
                                                    <h3>You're reviewing: Tristique ut lectus</h3>
                                                    <h4>How do you rate this product? *</h4>
                                                    <form method="post" action="https://demo.hasthemes.com/mozar-preview/mozar/mail.php">
                                                        <div class="p-details-table table-responsive">
                                                            <table>
                                                                <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th><span>1 star</span></th>
                                                                        <th><span>2 stars</span></th>
                                                                        <th><span>3 stars</span></th>
                                                                        <th><span>4 stars</span></th>
                                                                        <th><span>5 stars</span></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Price</th>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Value</th>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Quality</th>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                        <td><input type="radio" value="" class="radio"></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>    
                                                        </div>
                                                        <div class="form-top">
                                                            <div class="row">
                                                                <div id="review-form">
                                                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                                        <label>Nickname<span class="required" title="required">*</span></label>
                                                                        <input type="text" name="name" class="form-control">
                                                                    </div>
                                                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                                        <label>Summary of Your Review<span class="required" title="required">*</span></label>
                                                                        <input type="text" name="subject" class="form-control">
                                                                    </div>
                                                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                                                        <label>Review<span class="required" title="required">*</span></label>
                                                                        <textarea name="message" class="yourmessage"></textarea>
                                                                    </div>
                                                                </div>    
                                                            </div>    
                                                        </div>
                                                        <div class="buttons-set">
                                                            <button class="button" type="submit"><span>Submit Review</span></button>
                                                        </div>
                                                    </form>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tags" class="tab-pane" role="tabpanel">
                                        <div class="product-tag-name">
                                            <form method="post" action="https://demo.hasthemes.com/mozar-preview/mozar/mail.php">
                                                <div class="form-top">
                                                    <div class="row">
                                                        <div class="form-group col-md-12 col-lg-12 col-xl-12">
                                                            <label>Add Your Tags:</label>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="col-lg-12">
                                                            <button class="button" type="button">
                                                                <span>Add Tags</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <p>Use spaces to separate tags. Use single quotes (') for phrases.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-carousel-area section-top-padding">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="section-title"><h2>SẢN PHẨM LIÊN QUAN</h2></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="upsell-product-details-carousel">
                                    <?php
    $a_sid=array();
		foreach($_SESSION['ss'] as $k=>$v)
		{
		  $a_sid[$k]=$v['ssid'];
		}
		for ($j = 3, $count_spmoi = count($sanpham_khac); $j < $count_spmoi; $j++) { ?> 
                                    
                                    <div class="col-lg-12">
                                        <div class="single-product-item">
                                            <div class="single-product clearfix">
                                                <a href="#">
                                                    <span class="product-image">
                                                        <img src="thumb/540x728/1/<?php
             if ($sanpham_khac[$j]['photo'] != NULL)
                 echo _upload_product_l . $sanpham_khac[$j]['photo'];
             else
                 echo 'images/no-image-available.png'; ?>"
        alt="<?= $sanpham_khac[$j]["ten_$lang"] ?>" />
                                                    </span>
                                                </a>
                                            </div>
                                            <h2 class="single-product-name"><a href="#"><?= $sanpham_khac[$j]["ten_$lang"] ?></a></h2>
                                            <div class="price-box">
                                                <p class="special-price">
                                                    <span class="price"><?= $sanpham_khac[$j]['gia_vnd'] ?></span>vnđ
                                                </p> 											
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php } ?>
                                  
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="col-xl-3 col-lg-3">
                        <div class="single-products-category">
                            <div class="section-title"><h2>SẢN PHẨM MỚI</h2></div>
                            <div class="category-products">
                                <?php
                                     $a_sid=array();
	                                    	foreach($_SESSION['ss'] as $k=>$v)
	                                        	{
	                                            	  $a_sid[$k]=$v['ssid'];
	                                        	}
	                                	for ($j = 0, $count_spmoi = count($sanpham_khac); $j < 3; $j++) { ?> 
                                
                                <div class="product-items">
                                    <div class="p-category-image">
                                        <a href="#">
                                            <img alt="" src="thumb/80x100/1/<?php
                                                  if ($sanpham_khac[$j]['photo'] != NULL)
                                                               echo _upload_product_l . $sanpham_khac[$j]['photo'];
                                                  else
                                                                 echo 'images/no-image-available.png'; ?>"
                                                          alt="<?= $sanpham_khac[$j]["ten_$lang"] ?>" />
                                        </a>
                                    </div>
                                    <div class="p-category-text">
                                        <h2 class="category-product-name"><a href="#"><?= $sanpham_khac[$j]["ten_$lang"] ?></a></h2>
                                        <div class="price-box">
                                            <p class="special-price">
                                                <span class="price"><?= $sanpham_khac[$j]['gia_vnd'] ?></span> vnđ
                                            </p> 											
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                
                                
                                
                            </div>
                        </div>
                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Tags Tìm Kiếm</h2></div>
                            <div class="popular-tags">
                                <ul class="tag-list">                  
                                    <li><a href="#">bộ Bé Trai</a></li>
                                    <li><a href="#">bộ Bé Gái</a></li>
                                    <li><a href="#">bộ mặc nhà</a></li>
                                    <li><a href="#">bộ dã ngoại</a></li>
                                    <li><a href="#">bộ lót bé gái</a></li>
                                    <li><a href="#">bộ lót bé trai</a></li>
                                    <li><a href="#">quần lót bé gái</a></li>
                                    <li><a href="#">quần lót bé trai</a></li>
                                    <li><a href="#">áo thun dây</a></li>
                                    <li><a href="#">áo ba lỗ</a></li>
                                    <li><a href="#">set mặc nhà</a></li>
                                    <li><a href="#">set t-shirt</a></li>
                                    <li><a href="#">set ba lỗ</a></li>
                                </ul>
                             <!--   <div class="tag-actions">
                                    <a href="#">View All Tags</a>
                                </div> -->
                            </div>
                        </div>
                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Compare Products </h2></div>
                            <div class="block-content">
                                <p class="empty">You have no items to compare.</p>
                            </div>
                        </div>
                        <div class="sidebar-content">
                            <div class="banner-box">
                                <a href="#"><img alt="" src="img/banner/14.jpg"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End of Product Details Area-->
        <!--Brand Area Strat-->
        <!-- <div class="brand-area">
            <div class="container">
                <div class="brand-content">
                    <div class="row">
                        <div class="brand-carousel">
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/3.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/4.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/5.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/6.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="single-brand">
                                    <a href="#">
                                        <img src="img/brand/6.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div> -->
        <!--End of Brand Area-->
        <!--Service Area Start-->
        <!-- <div class="service-area">
            <div class="container">
                <div class="service-padding">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-service mb-sm-30">
                                <span class="fa fa-truck"></span>
                                <h3>fREE SHIPPING</h3>
                                <p>One order over $99</p>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-service mb-sm-30">
                                <span class="fa fa-dropbox"></span>
                                <h3>Special gift cards</h3>
                                <p> Give the perfect gift</p>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-service">
                                <span class="fa fa-calendar-o"></span>
                                <h3>Daily promotion</h3>
                                <p> Set up perspiciatis unde</p>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-service">
                                <span class="fa fa-comments-o"></span>
                                <h3>Special gift cards</h3>
                                <p> Give the perfect gift</p>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div> -->
        <!--End of Service Area-->
        <!--Footer Widget Area Start-->
        <div class="footer-widget-area">
            <div class="container">
                <div class="footer-widget-padding"> 
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget mb-sm-30">
                                <div class="footer-widget-title">
                                    <h3>our support</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <ul>
                                        <li><a href="#">Sitemap</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                        <li><a href="#">Your Account</a></li>
                                        <li><a href="#">Advanced Search</a></li>
                                        <li><a href="#">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget mb-sm-30">
                                <div class="footer-widget-title">
                                    <h3>My account</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <ul>
                                        <li><a href="#">My Account</a></li>
                                        <li><a href="#">Order History</a></li>
                                        <li><a href="#">Returns</a></li>
                                        <li><a href="#">Specials</a></li>
                                        <li><a href="#">Site Map</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget">
                                <div class="footer-widget-title">
                                    <h3>Contact us</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <ul class="address">
                                        <li><span class="fa fa-map-marker"></span> Addresss: Lorem ipsum dolor</li>
                                        <li><span class="fa fa-phone"></span> (800) 0123 4567 890</li>
                                        <li class="support-link"><span class="fa fa-envelope-o"></span> admin@bootexperts.com</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget">
                                <div class="footer-widget-title">
                                    <h3>Sign up for newsletter</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <form id="newsletter" method="post" action="#">
                                        <div class="newsletter-content">
                                           <input type="text" name="email">
                                            <button class="button" type="submit"><span>Subscribe</span></button>
                                        </div>
                                    </form>
                                    <ul class="social-link">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
        <!--End of Footer Widget Area-->
        <!--Footer Area Start-->
        <footer class="footer">
            <div class="container">
                <div class="footer-padding">   
                    <div class="row">
                        <div class="col-xl-7 col-lg-7 col-md-8">
                            <nav>
                                <ul id="footer-menu">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                                <p class="author">Copyright © 2016 <a href="#">BootExperts</a> All Rights Reserved.</p>
                            </nav>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-4">
                            <p class="payment-image">
                                <img src="img/payment.png" alt="">
                            </p>
                        </div>
                    </div>
                </div>    
            </div>
        </footer>
        <!--End of Footer Area-->
        
		<!-- jquery
		============================================ -->		
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
        
		<!-- popper JS
		============================================ -->		
        <script src="js/popper.min.js"></script>
        
		<!-- bootstrap JS
		============================================ -->		
        <script src="js/bootstrap.min.js"></script>
        
        <!-- nivo slider js
		============================================ -->       
		<script src="lib/nivo-slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
		<script src="lib/nivo-slider/home.js" type="text/javascript"></script>
		
		<!-- wow JS
		============================================ -->		
        <script src="js/wow.min.js"></script>
        	
		<!-- meanmenu JS
		============================================ -->		
        <script src="js/jquery.meanmenu.js"></script>
        
		<!-- owl.carousel JS
		============================================ -->		
        <script src="js/owl.carousel.min.js"></script>
        
		<!-- price-slider JS
		============================================ -->		
        <script src="js/jquery-price-slider.js"></script>	
        
		<!-- scrollUp JS
		============================================ -->		
        <script src="js/jquery.scrollUp.min.js"></script>
        
        <!--Countdown js-->
        <script src="js/jquery.countdown.min.js"></script>
        
		<!-- Sticky Menu JS
		============================================ -->		
        <script src="js/jquery.sticky.js"></script>
        
		<!-- Elevatezoom JS
		============================================ -->		
        <script src="js/jquery.elevateZoom-3.0.8.min.js"></script>
        
		<!-- plugins JS
		============================================ -->		
        <script src="js/plugins.js"></script>
        
		<!-- main JS
		============================================ -->		
        <script src="js/main.js"></script>
    </body>

<!-- Mirrored from demo.hasthemes.com/mozar-preview/mozar/product-details.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 07 Jan 2020 03:41:40 GMT -->
</html>