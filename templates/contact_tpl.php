<!--Contact Us Area Start-->
<div class="contact-us-area area-padding">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-3">
                <div class="sidebar-content">
                    <div class="section-title no-margin"><h2>Tags Tìm Kiếm</h2></div>
                    <div class="popular-tags">
                        <ul class="tag-list">                  
                            <li><a href="#">bộ Bé Trai</a></li>
                            <li><a href="#">bộ Bé Gái</a></li>
                            <li><a href="#">bộ mặc nhà</a></li>
                            <li><a href="#">bộ dã ngoại</a></li>
                            <li><a href="#">bộ lót bé gái</a></li>
                            <li><a href="#">bộ lót bé trai</a></li>
                            <li><a href="#">quần lót bé gái</a></li>
                            <li><a href="#">quần lót bé trai</a></li>
                            <li><a href="#">áo thun dây</a></li>
                            <li><a href="#">áo ba lỗ</a></li>
                            <li><a href="#">set mặc nhà</a></li>
                            <li><a href="#">set t-shirt</a></li>
                            <li><a href="#">set ba lỗ</a></li>
                        </ul>
                        <!--   <div class="tag-actions">
                            <a href="#">View All Tags</a>
                        </div> -->
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="banner-box">
                        <a href="#"><img src="img/banner/14.jpg" alt=""></a>
                    </div>
                </div>
                <div class="sidebar-content">
                    <div class="section-title no-margin"><h2>ALBUMS MỚI </h2></div>
                    <div class="block-content">
                        <p class="empty">Cập nhật mẫu mới nhất hiện nay</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-9">
                <div class="contact-us-area">
                    <!-- google-map-area start -->
                    <div class="google-map-area">
                        <!--  Map Section -->
                        <div id="contacts" class="map-area">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.9606835679697!2d106.83201701447709!3d10.814320761450972!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3175213b4a78ec57%3A0xe1195d6a0e656bed!2zMTgwMCDEkMaw4budbmcgTmd1eeG7hW4gRHV5IFRyaW5oLCBUcsaw4budbmcgVGjhuqFuaCwgUXXhuq1uIDksIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1604823414213!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            <!--
                            <div id="googleMap" style="width:100%;height:430px;"></div>
                            -->
                        </div>
                    </div>
                    <!-- google-map-area end -->
                    <!-- contact us form start -->
                    <div class="contact-us-form">
                        <div class="page-title">
                            <h1>Điền thông tin liên hệ với chúng tôi tại đây</h1>
                        </div>
                        <div class="contact-form">
                            <span class="legend">Thông tin của bạn</span>
                            <form action="https://demo.hasthemes.com/mozar-preview/mozar/mail.php" method="post">
                                <div class="form-top">
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <label>Tên<span class="required" title="required">*</span></label>
                                        <input name="name" type="text" class="form-control">
                                    </div>
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <label>Email<span class="required" title="required">*</span></label> 
                                        <input name="email" type="email" class="form-control">
                                    </div>										
                                    <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                        <label>Điện thoại<span class="required" title="required">*</span></label>
                                        <input name="number" type="text" class="form-control">
                                    </div>	
                                    <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                        <label>Nội dung<span class="required" title="required">*</span></label>
                                        <textarea name="message" class="yourmessage"></textarea>
                                    </div>												
                                </div>
                                <div class="submit-form form-group col-sm-12 submit-review">
                                    <p class="floatright"><sup>*</sup> Quan Trọng</p>
                                    <div class="clearfix"></div>
                                    <button class="button floatright" type="submit"><span>Gửi đi</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- contact us form end -->
                </div>					
            </div>
        </div>
    </div>
</div>