vnTProduct = {
	
	check_all:function()	
	{		
		var c = $("#all").attr('checked');
		
		$("#List_Product").find('input:checkbox' ).attr( 'checked', function() {
			var item_id = 'item'+$(this).val();
			if (c){
				$('#'+item_id).addClass('item_select')	;
				return 'checked';
			}else{
				$('#'+item_id).removeClass('item_select')	;	
				return '';	
			}
		}); 
				
	},
	
	select_item:function(id)	
	{
		var item_id = 'item'+id;
		var c = $("#"+item_id+" #ch_id").attr('checked');
		if (c){
			$('#'+item_id).addClass('item_select')	;
		}else{
			$('#'+item_id).removeClass('item_select')	;	
		}
		  
	},	
		
	FlyItem:function (IDcontrolFly,left, height, opacity, maxwidth,quantity) {
    var IMG = $("#" + IDcontrolFly + " img");
    $("body #ImgSC").remove();
    var tt = IMG.attr("src");
    $("body").append("<img id=\"ImgSC\" style=\"position:fixed; z-index:999; filter:alpha(opacity=" + opacity * 100 + " ); opacity:" + opacity + "; max-width:" + maxwidth + "px ; left:" + left + "px; top:" + height + "px \"; src=\"" + IMG.attr("src") + "\"/>");
    left += 20;
    height = MheightI - ((left * MheightI) / MwidthS) + 20;
    opacity -= 0.02;
    maxwidth -= 5;
    if (left < MwidthS) {
        var timer = setTimeout("vnTProduct.FlyItem('" + IDcontrolFly + "'," + left + "," + height + "," + opacity + "," + maxwidth + "," + quantity + ")", 20);
    }
    else {
       $("body #ImgSC").remove();        
			 $("#ext_numcart").text(parseInt($("#ext_numcart").text()) + quantity);
    }
	},
	
	//do_AddItemFlyCart 
	do_AddItemFlyCart:function (idControlS,id) {
 		
		var quantity = $("#quantity").val();  
		if(quantity =='undefined') quantity = 1;		
		quantity = parseInt(quantity);
					
 	  var mydata =  'id='+ id+'&quantity='+quantity;  
		
		$.ajax({
			async: true,
			dataType: 'json',
			url: ROOT+"modules/product/ajax/ajax.php?do=add_cart",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				if(data.ok == 1)	{
					 $(document).scrollTop(0); 
					MwidthS = $("#ext_numcart").offset().left;
					MheightS = $("#ext_numcart").offset().top;
					if (typeof ($("#" + idControlS + " img")) != 'undefined') {
							MwidthI = $("#" + idControlS + " img").offset().left;
							MheightI = $("#" + idControlS + " img").offset().top;
					}										  
					vnTProduct.FlyItem(idControlS, MwidthI, MheightI, 1, 300,quantity); 						
				}	else {
					jAlert(data.mess,'Báo lỗi');
				}	 
 				
			}
		})
	},
	 
	
	
	DoAddCart:function (){ 
		var aList = new Array;
		var count=0;
		$("#List_Product :input:checkbox").each( function() {
				if( $(this).attr('checked') && $(this).attr('id')!="all" ){
					aList.push($(this).val());
					count=count+1;
				} 																																 
		} );		
		
 		if (count==0){ jAlert('Vui lòng chọn 1 sản phẩm', 'Báo lỗi') }
 		else {
			p_id = aList.join(',');	
			location.href=ROOT+'san-pham/cart.html/?do=add&pID='+p_id;	
		}
	  
		return false;
	
	},
	
	DoCompare:function (){
		var catRoot =  $("#catRoot").val();
		var aList = new Array;
		var count=0;
		$("#List_Product :input:checkbox").each( function() {
				if( $(this).attr('checked') && $(this).attr('id')!="all" ){
					aList.push($(this).val());
					count=count+1;
				} 																																 
		} );
		
		
		if(catRoot =='') { jAlert('Không thể so sánh do không cùng chủng loại', 'Báo lỗi')}
		else if (count==0){ jAlert('Vui lòng chọn 1 sản phẩm để so sánh', 'Báo lỗi') }
		else if (count>3){ jAlert('Vui lòng chọn tối đa 3 sản phẩm để so sánh', 'Báo lỗi') }
		else {
			p_id = aList.join(',');	
			location.href=ROOT+'san-pham/compare_product.html/?catRoot='+catRoot+'&p_id='+p_id;	
		}
	  
		return false;
	
	},

	do_WishList:function (doAction,id) {
 
 		 
		var mydata =  "act="+doAction +'&id='+id; 
		$.ajax({
			async: true,
			dataType: 'json',
			url: ROOT+"modules/product/ajax/ajax.php?do=wishList",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				jAlert(data.mess, 'Thông báo');					    
			}
		}) 
		 
		return false ;
	},
	 
	do_DelWishList:function () 
	{
 
 		var aList = new Array;
		var count=0;
		$("#List_Product :input:checkbox").each( function() {
				if( $(this).attr('checked') && $(this).attr('id')!="all" ){
					aList.push($(this).val());
					count=count+1;
				} 																																 
		} );		
		
 		if (count==0){ jAlert('Vui lòng chọn 1 sản phẩm', 'Báo lỗi') }
 		else {
			p_id = aList.join(',');	
			location.href=ROOT+'san-pham/wishlist.html/?do=del&delID='+p_id;	
		}	  
		return false;
	} ,
	
	loadOptionCategory:function (cid,lang,ext_display) 
	{
    var mydata =  "cid="+ cid +'&lang='+lang; 
		$.ajax({
			async: true,
			dataType: 'json',
			url: ROOT+"modules/product/ajax/ajax.php?do=optionBrand",
			type: 'POST',
			data: mydata ,
			success: function (data) {
				$("#"+ext_display).html(data.html) ;
			}
		}) 
		 
		return false ;
	},
	
	do_ToggleProject:function () 
	{
 		$("#ext_toggle_project").slideToggle(150) ;
		return false ;
	}  
	
	
	
	
};
 
 

function format_number (num) {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.round(num*100+0.50000000001);
	num = Math.round(num/100).toString();
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num);
}

 
function load_image(src,src_big)
{
	/*$("#divImage img").fadeTo('slow', 0.1, function() {
		$(this).attr( "src", src ).animate({ opacity: 1.0 }, 500) ;
	});*/
	$("#divImage img").attr( "src", src );
	
	$("#divImage a").attr( "href", src_big ) ;
}  

function numberFormat(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1))
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	return x1 + x2;
}

function formatCurrency(div_id,str_number){
	/*Convert tu 1000->1.000*/
	/*var mynumber=1000;str_number = str_number.replace(/\./g,"");*/
	document.getElementById(div_id).innerHTML = '<font color=blue>' + numberFormat(str_number) + '<font>'; 
	document.getElementById(div_id).innerHTML = document.getElementById(div_id).innerHTML + ' <font color=red>VND</font>';
} 
 
var oscDebug = false;
var propertyHolidays = [[1, 1, 2012], [5, 16, 2012], [5, 17, 2012], [5, 28, 2012], [7, 4, 2012], [9, 3, 2012], [11, 22, 2012], [11, 23, 2012], [12, 25, 2012]];
  
// TFS 199 start
function PopulateTourReserveTimes(tourDate, tourTimes, lastTourTimesMRI) {
//    var tourTimesTest = "10:00 AM,5:00 PM~8:30 AM,6:00 PM~8:30 AM,6:00 PM~8:30 AM,5:30 PM~8:30 AM,11:00 AM~8:30 AM,6:00 PM~10:00 AM,5:00 PM";
//    var tourTimesTest = "10:00 AM,5:00 PM~8:30 AM,6:00 PM~8:30 AM,6:00 PM~8:30 AM,5:30 PM~8:30 AM,10:00 PM~8:00 AM,6:00 PM~10:00 AM,5:00 PM";
//    var tourTimesTest = "appt,appt~8:30 AM,6:00 PM~8:30 AM,6:00 PM~8:30 AM,6:00 PM~8:30 AM,6:00 PM~appt,appt~10:00 AM,5:00 PM";
//    var tourTimesTest = "closed,closed~10:00 AM,6:00 PM~9:00 AM,6:00 PM~10:00 AM,6:00 PM~9:00 AM,6:00 PM~closed,closed~10:00 AM,5:00 PM";

//    var hoursForDay = tourTimesTest.split("~");

// paste below here
    // get the tour hours for this day
    var hoursForDay = tourTimes.split("~");
    var inputIndex = tourDate.getDay();
    var openAndClose = hoursForDay[inputIndex].split(",");
    var openTimeRaw = openAndClose[0];
    var closeTimeRaw = openAndClose[1];
    var finalTourTimesList;
    if (openTimeRaw == "closed") {
        finalTourTimesList = InformNoTours("Office closed on this day");
    }
    else {
        if (openTimeRaw == "appt") {
            var foundGoodDay = false;
            var dayOrderToCheck = [0, 6, 5, 1, 2, 3, 4]; // Sunday, Saturday, Friday, Monday, Tues - Thursday
            var dayToCheckIndex = 0;

            while (!foundGoodDay && (dayToCheckIndex <= 6)) {
                var altIndex = dayOrderToCheck[dayToCheckIndex];
                var altOpenAndClose = hoursForDay[altIndex].split(",");
                var altOpenTimeRaw = altOpenAndClose[0];
                if (altOpenTimeRaw != "appt" && altOpenTimeRaw != "closed") {
                    // regular office houes
                    openTimeRaw = altOpenAndClose[0];
                    closeTimeRaw = altOpenAndClose[1];
                    foundGoodDay = true;
                }
                dayToCheckIndex++;
            }
        }

        // determine the day of the tourDate
        var inputBuffer = new Date(tourDate.getFullYear(), tourDate.getMonth(), tourDate.getDate());
        var openTimeString = inputBuffer.toString().replace("00:00:00", openTimeRaw);
        var openTimeDate = new Date(openTimeString);
        var closeTimeString = inputBuffer.toString().replace("00:00:00", closeTimeRaw);
        var closeTimeDate = new Date(closeTimeString);
        var firstTour = new Date();
        var lastTour = new Date();

        var now = new Date();
        var currentDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        var currentTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes());
        var currentHour = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours());

        var businessDayQuantity;
        var earliestTour;
        var latestTour;

        if (currentDay.toString() == tourDate.toString()) {
            // 1. trying set the appointment for TODAY
            // 1a. minutes of earliest tour time will be rounded up to nearest 30 minutes.
            var earliestMinutes = currentTime.getMinutes();
            var minutesAdjustment = 0;
            if (earliestMinutes != 0) {
                for (var goodMinutes = 30; goodMinutes <= 60; goodMinutes += 30) {
                    if (earliestMinutes <= goodMinutes) {
                        minutesAdjustment = goodMinutes - earliestMinutes;
                        break;
                    }
                }
            }
            var adjustedCurrent = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), currentTime.getHours(), currentTime.getMinutes() + minutesAdjustment);
           
            if (adjustedCurrent < openTimeDate) {
                // 1ai. If the current time < Office Open Time, then 6 hours from the Office Open Time
                earliestTour = new Date(openTimeDate.getFullYear(), openTimeDate.getMonth(), openTimeDate.getDate(), openTimeDate.getHours() + 6, openTimeDate.getMinutes());
            }
            else {
                // 1aii.If the current time => Office Open Time, then 6 hours from the current time
                earliestTour = new Date(adjustedCurrent.getFullYear(), adjustedCurrent.getMonth(), adjustedCurrent.getDate(), adjustedCurrent.getHours() + 6, adjustedCurrent.getMinutes());
            }

            latestTour = GetLatestTour(now, closeTimeDate, lastTourTimesMRI);
        }
        else {
            if (days_between(tourDate, currentDay) < 2) {
                // default is next business day or user chose next business day
                businessDayQuantity = 1;
                earliestTour = GetEarliestTour(hoursForDay, openTimeDate, businessDayQuantity);
            }
            else {
                // default is 2 or more days out. may be business or closed days
                // get business days
                businessDayQuantity = GetBusinessDayQuantity(tourDate, currentDay, hoursForDay);
                if (businessDayQuantity == 1) {
                    // follow next bus day rules
                    earliestTour = GetEarliestTour(hoursForDay, openTimeDate, businessDayQuantity);
                }
                else {
                    // 3. Setting the appointment for 2 OR MORE BUSINESS DAYS IN THE FUTURE
                    // a. earliest tour will be office open
                    earliestTour = openTimeDate;
                }
            }
            // 2d and 3b. Latest tour must be at least 30 minutes before Last Tour or Office Close, whichever is earlier
            latestTour = GetLatestTour(tourDate, closeTimeDate, lastTourTimesMRI);
        } 
    }

    // fill drop down start
    var uxTourTime = document.getElementById("uxTourTime");

     
    // fill drop down end

}

function GetDefaultTourDate(tourTimes, lastTourTimesMRI) {
 
    var now = new Date();
    var initTour = new Date(now.getFullYear(), now.getMonth(), now.getDate());

    // get tour hours for today
    var hoursForDay = tourTimes.split("~");
    var inputIndex = initTour.getDay();
    var openAndClose = hoursForDay[inputIndex].split(",");
    var openTimeRaw = openAndClose[0];
    var closeTimeRaw = openAndClose[1];

    if (openTimeRaw == "closed" || isPropertyHolidayToday(initTour, propertyHolidays) == true) {
        return GetNextDate(initTour, hoursForDay);
    }

    // not closed today so try to use today as the tour date
    tourDate = initTour;

    if (openTimeRaw == "appt") {
        var foundGoodDay = false;
        var dayOrderToCheck = [0, 6, 5, 1, 2, 3, 4]; // Sunday, Saturday, Friday, Monday, Tues - Thursday
        var dayToCheckIndex = 0;

        while (!foundGoodDay && (dayToCheckIndex <= 6)) {
            var altIndex = dayOrderToCheck[dayToCheckIndex];
            var altOpenAndClose = hoursForDay[altIndex].split(",");
            var altOpenTimeRaw = altOpenAndClose[0];
            if (altOpenTimeRaw != "appt" && altOpenTimeRaw != "closed") {
                // regular office houes
                openTimeRaw = altOpenAndClose[0];
                closeTimeRaw = altOpenAndClose[1];
                foundGoodDay = true;
            }
            dayToCheckIndex++;
        }
    }

    var finalTourTimesList;
    finalTourTimesList = InformNoTours("Office closed on this day");

    // determine the day of the tourDate
    var inputBuffer = new Date(tourDate.getFullYear(), tourDate.getMonth(), tourDate.getDate());
    var openTimeString = inputBuffer.toString().replace("00:00:00", openTimeRaw);
    var openTimeDate = new Date(openTimeString);
    var closeTimeString = inputBuffer.toString().replace("00:00:00", closeTimeRaw);
    var closeTimeDate = new Date(closeTimeString);
    var firstTour = new Date();
    var lastTour = new Date();

    var now = new Date();
    var currentDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
    var currentTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes());
    var currentHour = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours());

    if (currentDay.toString() == tourDate.toString()) {
        // trying to get tour today

        // 1a. minutes of earliest tour time will be rounded up to nearest 30 minutes.
        var earliestMinutes = currentTime.getMinutes();
        var minutesAdjustment = 0;
        if (earliestMinutes != 0) {
            for (var goodMinutes = 30; goodMinutes <= 60; goodMinutes += 30) {
                if (earliestMinutes <= goodMinutes) {
                    minutesAdjustment = goodMinutes - earliestMinutes;
                    break;
                }
            }
        }
        var adjustedCurrent = new Date(currentTime.getFullYear(), currentTime.getMonth(), currentTime.getDate(), currentTime.getHours(), currentTime.getMinutes() + minutesAdjustment);

        var earliestTourToday;
        if (adjustedCurrent < openTimeDate) {
            // 1ai. If the current time < Office Open Time, then 6 hours from the Office Open Time
            earliestTourToday = new Date(openTimeDate.getFullYear(), openTimeDate.getMonth(), openTimeDate.getDate(), openTimeDate.getHours() + 6, openTimeDate.getMinutes());
        }
        else {
            // 1aii.If the current time => Office Open Time, then 6 hours from the current time
            earliestTourToday = new Date(adjustedCurrent.getFullYear(), adjustedCurrent.getMonth(), adjustedCurrent.getDate(), adjustedCurrent.getHours() + 6, adjustedCurrent.getMinutes());
        }

        // 1b. latest appointment is 30 minutes before (the Last Tour time or Office Close time, whichever is earlier)
        var lastTourListMRI = lastTourTimesMRI;
        var lastTourMRIIndex = now.getMonth();
        var lastTourTimeMRI = lastTourListMRI.split(",")[lastTourMRIIndex];

        var lastTourMRI;
        if (lastTourTimeMRI == "") {
            // no MRI last tour
            lastTourMRI = closeTimeDate;
        }
        else {
            var lastMRITourHourMins = lastTourTimeMRI.split(":");
            lastTourMRI = new Date(now.getFullYear(), now.getMonth(), now.getDate(), lastMRITourHourMins[0], lastMRITourHourMins[1]);
        }
        var latestTourToday;
        if (closeTimeDate <= lastTourMRI) {
            latestTourToday = new Date(closeTimeDate.getFullYear(), closeTimeDate.getMonth(), closeTimeDate.getDate(), closeTimeDate.getHours(), closeTimeDate.getMinutes() - 30);
        }
        else {
            latestTourToday = new Date(lastTourMRI.getFullYear(), lastTourMRI.getMonth(), lastTourMRI.getDate(), lastTourMRI.getHours(), lastTourMRI.getMinutes() - 30);
        }

        if (earliestTourToday <= latestTourToday) {
            // today is ok
            return tourDate;
        }
        else {
            // today is not ok
            return GetNextDate(initTour, hoursForDay);
        }
    }
    else {
        // today is not ok
        return GetNextDate(initTour, hoursForDay);
    }

}

function GetNextDate(initTour, hoursForDay) {
    // get next day
    for (var j = 1; j <= 6; j++) {
        //
        var nextDate = new Date(initTour.getFullYear(), initTour.getMonth(), initTour.getDate() + j);
        var nextDateIndex = nextDate.getDay();
        var nextDateOpenAndClose = hoursForDay[nextDateIndex].split(",");
        var nextDateOpenTimeRaw = nextDateOpenAndClose[0];
        if (nextDateOpenTimeRaw != "closed") {
            // good day found
            return nextDate;
        }
    }
}


function CreateTourTimes(firstTour, lastTour) {
    var startTime = firstTour.getTime();
    var endTime = lastTour.getTime();

    var tourDateAndTime = new Date();
    var tours = new Array();
    var tourIndex = 0;

    // loop from open to close incrementing loopTime by number of milliseconds in 30 minutes
    for (var loopTime = startTime; loopTime <= endTime; loopTime += (1000 * 60 * 30)) {
        tourDateAndTime = new Date(loopTime);

        var tourHour = tourDateAndTime.getHours();
        var amORpm = (tourHour < 12) ? amORpm = "AM" : amORpm = "PM";
        if (tourHour == 0) tourHour = 12;
        if (tourHour > 12) tourHour = tourHour - 12;

        var tourMinutes = tourDateAndTime.getMinutes();
        if (tourMinutes.toString().length == 1) tourMinutes = "0" + tourMinutes;

        tours[tourIndex] = tourHour + ":" + tourMinutes + " " + amORpm;
        tourIndex++;
    }
    return tours;
}

function days_between(date1, date2) {

    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime()
    var date2_ms = date2.getTime()

    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(date1_ms - date2_ms)

    // Convert back to days and return
    return Math.round(difference_ms / ONE_DAY)
}

function InformNoTours(msg) {
    var tours = new Array();
    var tourIndex = 0;
    tours[tourIndex] = msg;
    return tours;
}

function ChangeTourDate(date, inst) {
    var newTourDate = new Date(date);
    var newTourDateOnly = new Date(newTourDate.getYear(), newTourDate.getMonth(), newTourDate.getDate());
    PopulateTourReserveTimes(newTourDate, tourTimes, lastTourTimesMRI);

    // if empty drop down
    var uxTourTime = document.getElementById("uxTourTime"); 
    SetHiddenTourDate(date);
}

function GetClosedDays(tourTimes) {
    var closedDays = new Array();
    var closedDayIndex = 0;
    var hoursForDay = tourTimes.split("~");
    for (var dayIndex = 0; dayIndex < 7; dayIndex++) {
        var openAndClose = hoursForDay[dayIndex].split(",");
        var openTimeRaw = openAndClose[0];
        if (openTimeRaw == "closed") {
            closedDays[closedDayIndex] = dayIndex;
            closedDayIndex++
        }
    }
    return closedDays;
}

function HideOfficeClosedDays(date, inst) {
    var day = date.getDay();
    for (i = 0; i < officeClosedDays.length; i++) {
        if ($.inArray(day, officeClosedDays) != -1) {
            return [false];
        }
    }
    // TFS 242 start
    for (holIndex = 0; holIndex < propertyHolidays.length; holIndex++) {
        if (date.getMonth() == propertyHolidays[holIndex][0] - 1 &&
            date.getDate() == propertyHolidays[holIndex][1] &&
            date.getFullYear() == propertyHolidays[holIndex][2]) {
            return [false];
        }
    }
    // TFS 242 end
    return [true];
}

function SetHiddenTourDate(tourDate) {
    var inputDate = new Date(tourDate);
    var prepDefaultTourDate = inputDate.getMonth() + 1 + "/" + inputDate.getDate() + "/" + inputDate.getFullYear();
    var hdnTourDate = document.getElementById("hdnTourDate");
    hdnTourDate.value = prepDefaultTourDate;
}

function SetDefaultHiddenTourTime() {
    var DropDownTourTime = document.getElementById("uxTourTime");
    var SelectedText = DropDownTourTime.options[0].text;
    var hdnTourTime = document.getElementById("hdnTourTime");
    hdnTourTime.value = SelectedText;
}

function SetHiddenTourTime() {
    var DropDownTourTime = document.getElementById("uxTourTime");
    var SelectedValue = DropDownTourTime.value;
    var hdnTourTime = document.getElementById("hdnTourTime");
    hdnTourTime.value = SelectedValue;
}

function GetEarliestTour(hoursForDay, openTimeDate, businessDays) {
    var earliestTour;
    // 1 day
    if (businessDays == 1) {
        // 2. Setting the appointment for the NEXT OPEN BUSINESS DAY
        var now = new Date();
        var todayAt4 = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 16);

        var todayIndex = now.getDay();
        var todayOpenAndClose = hoursForDay[todayIndex].split(",");
        var openToday = (todayOpenAndClose[0] != "closed") ? true : false;

        if (openToday == true) {
            if (now < todayAt4) {
                // 2a. office open. current time is before 4pm. earliest tour will be 1 hour after the office open on next business day property is open
                earliestTour = new Date(openTimeDate.getFullYear(), openTimeDate.getMonth(), openTimeDate.getDate(), openTimeDate.getHours(), openTimeDate.getMinutes() + 60);
            }
            else {
                // 2b. office open. current time is after 4pm. earliest tour will be 1pm on next business day property is open
                earliestTour = new Date(openTimeDate.getFullYear(), openTimeDate.getMonth(), openTimeDate.getDate(), 13);
            }
        }
        else {
            // 2c. office closed. current time is any time. earliest tour will be 1pm on next business day property is open
            earliestTour = new Date(openTimeDate.getFullYear(), openTimeDate.getMonth(), openTimeDate.getDate(), 13);
        }
    }
    // 2 or more days
    else if (businessDays >= 2) {
        // 3. Setting the appointment for 2 OR MORE BUSINESS DAYS IN THE FUTURE
        // 3a. earliest tour will be office open
        earliestTour = openTimeDate;
    }
    return earliestTour;
}

function GetLatestTour(date, closeTimeDate, lastTourTimesMRI) {
    // latest appointment is 30 minutes before (the Last Tour time or Office Close time, whichever is earlier)
    var latestTour = new Date();
    var lastTourListMRI = lastTourTimesMRI;
    var lastTourMRIIndex = date.getMonth();
    var lastTourTimeMRI = lastTourListMRI.split(",")[lastTourMRIIndex];

    if (lastTourTimeMRI == "") {
        // no MRI last tour
        lastTourMRI = closeTimeDate;
    }
    else {
        var lastMRITourHourMins = lastTourTimeMRI.split(":");
        lastTourMRI = new Date(date.getFullYear(), date.getMonth(), date.getDate(), lastMRITourHourMins[0], lastMRITourHourMins[1]);
    }
    
    if (closeTimeDate <= lastTourMRI) {
        latestTour = new Date(closeTimeDate.getFullYear(), closeTimeDate.getMonth(), closeTimeDate.getDate(), closeTimeDate.getHours(), closeTimeDate.getMinutes() - 30);
    }
    else {
        latestTour = new Date(lastTourMRI.getFullYear(), lastTourMRI.getMonth(), lastTourMRI.getDate(), lastTourMRI.getHours(), lastTourMRI.getMinutes() - 30);
    }
    return latestTour;
}

function GetBusinessDayQuantity(tourDate, currentDay, hoursForDay) {
    // get number of days between the two
    var daysUntil = days_between(tourDate, currentDay);

    var maxDays = (daysUntil <= 14) ? daysUntil : 14;
    var businessDays = 0;
    // loop through count how may days office is not closed
    for (var dayCount = 1; dayCount <= maxDays; dayCount++) {
        // get day
        var nextDate = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate() + dayCount);
        var nextDateIndex = nextDate.getDay();
        var nextDateOpenAndClose = hoursForDay[nextDateIndex].split(",");
        var nextDateOpenTimeRaw = nextDateOpenAndClose[0];
        if (nextDateOpenTimeRaw != "closed" && !isPropertyHolidayToday(nextDate)) {
            // if not closed increment
            businessDays++;
        }
    }
    return businessDays;
}
// TFS 199 end 


// TFS 228
function GetFormAction(thisDocument) {
    var actionVal = "";
    if (thisDocument != null) {
        actionVal = thisDocument.location.pathname + thisDocument.location.search;
    }
    return actionVal;
}
// TFS 242 start
function isPropertyHolidayToday(today) {
    for (holIndex = 0; holIndex < propertyHolidays.length; holIndex++) {
        if (today.getMonth() == propertyHolidays[holIndex][0] - 1 &&
                today.getDate() == propertyHolidays[holIndex][1] &&
                today.getFullYear() == propertyHolidays[holIndex][2]) {
            return true;
        }
    }
    return false;
}
// TFS 242 end