<?php
error_reporting(0);
session_start();
$session = session_id();

@define('_template', './templates/');


@define('_source', './sources/');
@define('_lib', './libraries/');
@define(_upload_folder, './media/upload/');


include_once _lib . "config.php";

//Lưu ngôn ngữ chọn vào $_SESSION
$lang_arr = array("vi", "en", "cn", "ge");
if (isset($_GET['lang']) == true) {
    if (in_array($_GET['lang'], $lang_arr) == true) {
        $lang = $_GET['lang'];
        $_SESSION['lang'] = $lang;
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {

    $lang = $config["lang_default"];
}

require_once _source . "lang_$lang.php";

include_once _lib . "constant.php";
include_once _lib . "functions.php";
include_once _lib . "functions_giohang.php";
include_once _lib . "library.php";
include_once _lib . "class.database.php";
include_once _lib . "file_requick.php";
include_once _source . "counter.php";
include_once _source . "useronline.php";

$d = new database($config['database']);


if ($_REQUEST['command'] == 'add' && $_REQUEST['productid'] > 0) {
    $pid = $_REQUEST["productid"];
    $q = isset($_GET['quality']) ? ($_GET['quality']) : "1";
    addtocart($pid, $q);
    redirect(base_url("gio-hang"));
}
?>


<html lang="en-US" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"><!--<![endif]-->
    <head>
        <base href="<?= base_url() ?>/" />
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php if (isset($title_bar))
    echo $title_bar;
else
    echo $row_setting["title_$lang"];
?></title>
        <meta name="viewport" content="width=1300, initial-scale=1.0">
        <meta name="robots" content="index, follow" />


        <meta name="author" content="<?= $row_setting["author_web"] ?>">
        <meta name="keywords" content="<?= $row_setting["keywords_$lang"] ?>" />
        <meta name="description" content="<?= $row_setting["description_$lang"] ?>" />

        <meta http-equiv="Content-Language" content="vi" />
        <meta name="Language" content="vietnamese" />

        <meta property="og:locale" content="vi_VN" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?= $title_bar ?>" />
        <meta property="og:image" content="<?= $image ?>" />
        <meta property="article:publisher" content="<?= $row_setting["fanpage"] ?>" />
        <meta property="og:site_name" content="<?= $row_setting["ten_$lang"] ?>"/>
        <meta property="og:url" content="<?= $url_web ?>" />
        <meta property="og:description" content="<?= $description_web ?>" />

        <meta itemprop="name" content="<?= $row_setting["title_$lang"] ?>">
        <meta property="twitter:title" content="<?= $title_bar ?>">
        <meta property="twitter:url" content="<?= $url_web ?>">
        <meta property="twitter:card" content="summary">    

        <?= $row_setting["geo_meta"]; ?>


        <link rel="canonical" href="<?= getCurrentPageURL(); ?>" />	

        <link rel="shortcut icon" href="<?= _upload_hinhanh_l . $row_setting["favicon"] ?>">


        <?php include _template . "layout/style_web.php"; ?>


        <link rel='stylesheet' id='font-awesome-css'  href='fonts/font-awesome/css/font-awesome.min.css?ver=4.0.3' type='text/css' media='all' /> 
        <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->
        <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Open+Sans:400italic,700italic,300,400,600,700' rel='stylesheet' type='text/css'>



        <script type="text/javascript" src="js/jquery-1.7.2.js" ></script>

        <script type="text/javascript" src="js/my_srcipt_gaconit91_full.js" ></script>  
        <script type="text/javascript" src="js/jquery.hoverIntent.minified.js"></script>
        <script type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.cycle.all.js"></script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                $('.accordion-2').dcAccordion({
                    eventType: 'hover',
                    autoClose: false,
                    menuClose: true,
                    classExpand: 'dcjq-current-parent',
                    saveState: false,
                    disableLink: false,
                    showCount: false,
                    hoverDelay: 50,
                    speed: 'slow'
                });
                $('.fade').cycle();
            });
        </script>
        <script type="text/javascript" src="js/ddsmoothmenu.js"></script>

        <script type="text/javascript">
            ddsmoothmenu.init({
                mainmenuid: "smoothmenu1", //Menu DIV id
                orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
                classname: 'ddsmoothmenu', //class added to menu's outer DIV
                //customtheme: ["#804000", "#482400"],
                contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
            })
            ddsmoothmenu.init({
                mainmenuid: "smoothmenu2", //Menu DIV id
                orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
                classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
                //customtheme: ["#804000", "#482400"],
                contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
            })

        </script>
        <link rel="stylesheet" type="text/css" href="css/slick.css"/>
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
        <script type="text/javascript" src="js/slick.min.js"></script>

        <link rel="stylesheet" type="text/css" href="magiczoomplus/magiczoomplus.css">
        <script type="text/javascript" src="magiczoomplus/magiczoomplus.js"></script>
        <?= $row_setting["code_analytics"]; ?>



    <h1 style="display:none;"><?= $row_setting["h1_$lang"]; ?></h1>
    <h2 style="display:none;"><?= $row_setting["h2_$lang"]; ?></h2>

</head>    

<style type="text/css">
<?php
$d->reset();
$sql_mau = "select * from #_background where com='bg_header' ";
$d->query($sql_mau);
$bg_header_option = $d->fetch_array();
$bg = "";
if ($bg_header_option['chonbg'] == 1)
    $bg.=" url(" . _upload_background_l . $bg_header_option['photo'] . ")";

if ($bg_header_option['repeat1'] == 0)
    $bg.=" repeat";
else if ($bg_header_option['repeat1'] == 1)
    $bg.=" repeat-x";
else if ($bg_header_option['repeat1'] == 2)
    $bg.=" repeat-y";
else if ($bg_header_option['repeat1'] == 3)
    $bg.=" no-repeat";

if ($bg_header_option['vitri'] == 1)
    $bg.=" top";
else if ($bg_header_option['vitri'] == 2)
    $bg.=" right";
else if ($bg_header_option['vitri'] == 3)
    $bg.=" bottom";
else if ($bg_header_option['vitri'] == 4)
    $bg.=" left";
else if ($bg_header_option['vitri'] == 5)
    $bg.=" center";

if ($bg_header_option['vitri1'] == 1)
    $bg.=" top";
else if ($bg_header_option['vitri1'] == 2)
    $bg.=" right";
else if ($bg_header_option['vitri1'] == 3)
    $bg.=" bottom";
else if ($bg_header_option['vitri1'] == 4)
    $bg.=" left";
else if ($bg_header_option['vitri1'] == 5)
    $bg.=" center";

if ($bg_header_option['fixed'] == 1)
    $bg.=" fixed";


$d->reset();
$sql_mau = "select * from #_background where com='bg_footer' ";
$d->query($sql_mau);
$bg_footer_option = $d->fetch_array();
$bg_footer = "";
if ($bg_footer_option['chonbg'] == 1)
    $bg_footer.=" url(" . _upload_background_l . $bg_footer_option['photo'] . ")";

if ($bg_footer_option['repeat1'] == 0)
    $bg_footer.=" repeat";
else if ($bg_footer_option['repeat1'] == 1)
    $bg_footer.=" repeat-x";
else if ($bg_footer_option['repeat1'] == 2)
    $bg_footer.=" repeat-y";
else if ($bg_footer_option['repeat1'] == 3)
    $bg_footer.=" no-repeat";

if ($bg_footer_option['vitri'] == 1)
    $bg_footer.=" top";
else if ($bg_footer_option['vitri'] == 2)
    $bg_footer.=" right";
else if ($bg_footer_option['vitri'] == 3)
    $bg_footer.=" bottom";
else if ($bg_footer_option['vitri'] == 4)
    $bg_footer.=" left";
else if ($bg_footer_option['vitri'] == 5)
    $bg_footer.=" center";

if ($bg_footer_option['vitri1'] == 1)
    $bg_footer.=" top";
else if ($bg_footer_option['vitri1'] == 2)
    $bg_footer.=" right";
else if ($bg_footer_option['vitri1'] == 3)
    $bg_footer.=" bottom";
else if ($bg_footer_option['vitri1'] == 4)
    $bg_footer.=" left";
else if ($bg_footer_option['vitri1'] == 5)
    $bg_footer.=" center";

if ($bg_footer_option['fixed'] == 1)
    $bg_footer.=" fixed";
?>

<?php ?>
    /*		body{
                            background:<?= $bg_header_option['nenbackground'], $bg ?>,<?= $bg_footer_option['nenbackground'], $bg_footer ?>;
                    }*/
<?php ?>
</style>		
<body <?php if ($_GET["com"] == "lien-he") { ?> onLoad="initialize_contact(), initialize_footer();" <?php } else { ?> onload="initialize_footer();" <?php } ?>>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <div class="container_s">
        <div class="container-fluid pd0">
            <div id="main_header_s">
                <div class="row pd0 mg0 ">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pd0">
                        <?php include _template . "layout/header.php"; ?> 
                        <?php include _template . "layout/slideranh.php"; ?>
                    </div>
                </div>
            </div> 

            

            <div id="footer_s">
                <div class="container pd0">
                    <?php include _template . "layout/footer.php"; ?> 
                </div>
            </div>
        </div>
    </div>



    <?= $row_setting["codechat"]; ?>

</body>

</html>