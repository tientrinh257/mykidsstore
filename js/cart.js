$(document).ready(function() {
    $('#form-add-cart').on("submit", function(e) {
        e.preventDefault();
        var form = $(this);
        formdata = form.serialize();
        
        $.ajax({
            type: "POST",
            url: "ajax/add_cart.php",
            data: formdata,
            success: function(response){console.log(response);
                alert("đã thêm vào giỏ hàng");
                location.reload("#top");
            }          
        });
    });

    function numberFormat(num, ext) {
        ext = (!ext) ? '  VNĐ' : ext;
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + ext;
    }

    function updatePrice() {
        $tt = 0;
        $(".price_tt").each(function () {
            $h = $(this).html().replace(/\./g, "");
            //alert($h);
            $tt += parseInt($h);

        });
    }

    $(document).on('change', ".change_qty",function () {

        $val = parseInt($(this).val());
        $id = $(this).data("id");
        $sori = $(this).data("sori");
        
        if($val>1000){
        alert('Bạn đã vượt quá số lượng cho phép.');
        return false;
        }
        
        $tongri=$val;
        if (parseInt($val) < 1) {
            $(this).val(1);
            $val = 1;
        }
        $root = $(this).parents("tr");
        $price = parseInt($(this).data("price"));
        if ($price < 1) {
            $price = 0;
        }
        
        $root.find(".socai").html((parseInt($tongri)));
        $root.find(".price_tt").html(numberFormat(parseInt($price*$tongri)));
        $.ajax({
            url: "ajax/cart.php?type=update_qty",
            data: {"qty": $val, id: $id,sori: $sori},
            type: "post",
            success: function (data) {
                $(".get_order_total").html(numberFormat(data))
                updatePrice();
            }

        });
    });
});