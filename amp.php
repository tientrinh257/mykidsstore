<?php
@define('_source', './sources/');
@define('_lib', './libraries/');
@define(_upload_folder, './media/upload/');
include_once _lib . "config.php";
// https://demo.hasthemes.com/mozar-preview/mozar/index-3.html
//Lưu ngôn ngữ chọn vào $_SESSION
$lang = "vi";
require_once _source . "lang_$lang.php";
include_once _lib . "constant.php";
include_once _lib . "functions.php";
include_once _lib . "library.php";
include_once _lib . "class.database.php";
include_once _lib . "file_requick.php";
include_once _lib . "functions_giohang.php";
?>
<!Doctype html>
<html amp lang="vi">
    <head >
        <base href="<?= base_url() ?>/" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php
            if (isset($title_bar))
                echo $title_bar;
            else
                echo $row_setting["title_$lang"];
            ?>
        </title>
        <meta charset="utf-8">
	    <script async src="https://cdn.ampproject.org/v0.js"></script>
        <meta property="og:locale" content="vi_VN">
        <meta property="og:type" content="website">
        <meta property="og:title" content="MyKidsStore" />
        <meta property="og:url" content="https://mykidsstore.vn/">
        <meta property="og:site_name" content="MyKidsStore">
        <meta property="og:image" content="https://mykidsstore.vn/img/logo/logo.png">
        <meta property="og:description" content="<?= $row_setting["description_$lang"] ?>">
        <meta name="description" content="<?= $row_setting["description_$lang"] ?>">
        <meta name="keywords" content="<?= $row_setting["keywords_$lang"] ?>">
        <meta name="robots" content="index, follow">
        <meta name="author" content="mykidsstrore">
        <link rel="canonical" href="<?= getCurrentPageURL() ?>" >
        <title>MyKidsStore</title>

        <!-- Dublin Core -->
        <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
        <meta name="DC.title" content="<?= $title_bar ?>">
        <meta name="DC.identifier" content="<?= $url_web ?>">
        <meta name="DC.description" content="<?= $description_web ?>"/>
        <meta name="DC.subject" content="<?= $row_setting["keywords_$lang"] ?>">
        <meta name="DC.language" scheme="UTF-8" content="vi,en">

        <meta itemprop="name" content="<?= $row_setting["title_$lang"] ?>">
        <meta property="twitter:title" content="<?= $title_bar ?>"/>
        <meta property="twitter:url" content="<?= $url_web ?>"/>
        <meta property="twitter:card" content="summary"/>    

        <meta name="twitter:description" content="<?= $description_web ?>"/>
        <meta name="twitter:image" content="<?= $image ?>" />
        <meta property="article:author" content="<?= $url_web ?>" />
        <?= $row_setting["geo_meta"]; ?>

        <!-- Thêm cho Google Plus -->
        <meta name="author" content="mykidsstore" />
        <meta name="copyright" content="mykidsstore" />
        <meta name="robots" content="index,follow" />
        <meta name='revisit-after' content='1 days' />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script type="application/ld+json">
        {
        "@context": "https://schema.org",
        "@type": "LocalBusiness",
        "name": "MyKidsStore",
        "image": "https://mykidsstore.vn/img/logo/logo.png",
        "@id": "",
        "url": "https://mykidsstore.vn/",
        "telephone": "0788733999",
        "priceRange": "0",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "1800 Nguyễn Duy Trinh, Phường Trường Thành, Quận 9",
            "addressLocality": "Hồ Chí Minh",
            "postalCode": "700000",
            "addressCountry": "VN"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": 10.8078134,
            "longitude": 106.7784724
        },
        "openingHoursSpecification": {
            "@type": "OpeningHoursSpecification",
            "dayOfWeek": [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday"
            ],
            "opens": "00:00",
            "closes": "23:59"
        },
        "sameAs": "https://www.facebook.com/mykidsstoreHCM"
        }
        </script>
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <script custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async=""></script>
        <style amp-custom>
            <?php readfile( getcwd() . "/style.css"); ?>
            <?php readfile( getcwd() . "/w3.css"); ?>
            /* menu */
.logo {
  padding-left: 50px;
  padding-bottom: 60px;
  padding-top: 50px;
}
.navbar {
  padding-top: 0;
  padding-bottom: 0;
}
.navbar a {
  color: #000;
  font-weight: bold;
  text-decoration: none;
}
.navbar .nav-item {
  padding: 10px 15px ;
}
.navbar .nav-item .nav-link {
  color: #000;
  opacity: 1;
  font-size: 1.1rem;
}
.navbar .nav-item:hover {
  background-color: #f4b3ba;
}
.navbar .nav-item.active {
  background-color: #f4b3ba;
  position: relative;
  opacity: 1;
}
.navbar .active::before {
  content: "";
  border-left: 10px solid transparent;
  border-right: 10px solid transparent; 
  border-top: 10px solid #fff;
  position: absolute;
  top: 0;
  left: 50%;
  transform: translate(-50%,0);
}
.navbar-nav {
  width: 100%;
  justify-content: space-around;
  list-style-type: none;
  margin-top: 0;
  margin-bottom: 0;
}
/* end menu */
        </style>
    </head>

    <body>
        <header class="ampstart-headerbar flex justify-start items-center top-0 left-0 right-0 pl2 pr4 ">
            <div role="button" aria-label="open sidebar" on="tap:header-sidebar.toggle" tabindex="0"
                class="ampstart-navbar-trigger md-hide lg-hide pr2  ">☰
            </div>
            <!-- <a href="<?= base_url() ?>" class="text-decoration-none inline-block mx-auto ampstart-headerbar-home-link  ">
                <div class="ampstart-headerbar-title mx-auto lg-hide">
                    <amp-img class="w3-image" alt="logo" src="<?= base_url('img/logo/logo.png') ?>" width="100px"
                        height="100px"></amp-img>
                </div>
            </a>
            <nav class="navbar ampstart-headerbar-nav ampstart-nav xs-hide sm-hide black-background">
                <ul class="navbar-nav list-reset center m0 p0 flex nowrap">
                    <li class="ampstart-nav-item nav-item active"><a href="<?= base_url() ?>" class="nav-link text-decoration-none block">TRANG CHỦ</a></li>
                    <?php 
                        $d->query("select ten_$lang,tenkhongdau_$lang,id from #_product_list where hienthi=1 order by stt");
                        $list=$d->result_array();
                    ?>
                    <?php for($i=0;$i<count($list);$i++) { ?>
                        <li class="ampstart-nav-item nav-item">
                            <a href="<?=base_url().$list[$i]['tenkhongdau_vi']?>" title="<?=$list[$i]['ten_'.$lang]?>"><?=$list[$i]['ten_'.$lang]?></a>
                        </li>
                    <?php } ?>
                    <li class="ampstart-nav-item nav-item"><a href="#" class="nav-link text-decoration-none block">TIN TỨC</a></li>
                    <li class="ampstart-nav-item nav-item"><a href="#" class="nav-link text-decoration-none block">LIÊN HỆ</a></li>
                </ul>
            </nav> -->
        </header>

        <!-- Start Sidebar -->
        <amp-sidebar id="header-sidebar" class="ampstart-sidebar px3 md-hide lg-hide black-background" layout="nodisplay">
            <div class="flex justify-start items-center ampstart-sidebar-header">
                <div role="button" aria-label="close sidebar" on="tap:header-sidebar.toggle" tabindex="0"
                    class="ampstart-navbar-trigger items-start">✕</div>
            </div>
            <nav class="navbar ampstart-sidebar-nav ampstart-nav">
                <ul class="navbar-nav list-reset m0 p0 ampstart-label">
                    <li class="nav-item active ampstart-nav-item land-see-sidebar-nav-item ampstart-title-sm bold">
                        <a class="nav-link ampstart-nav-link" href="<?= base_url() ?>">TRANG CHỦ</a>
                    </li>
                    <?php 
                        $d->query("select ten_$lang,tenkhongdau_$lang,id from #_product_list where hienthi=1 order by stt");
                        $list=$d->result_array();
                    ?>
                    <?php for($i=0;$i<count($list);$i++) { ?>
                        <li class="ampstart-nav-item nav-item">
                            <a href="<?=base_url().$list[$i]['tenkhongdau_vi']?>" title="<?=$list[$i]['ten_'.$lang]?>"><?=$list[$i]['ten_'.$lang]?></a>
                        </li>
                    <?php } ?>
                    <li class="ampstart-nav-item nav-item"><a href="#" class="nav-link text-decoration-none block">TIN TỨC</a></li>
                    <li class="nav-item ampstart-nav-item land-see-sidebar-nav-item ampstart-title-sm bold">
                        <a class="nav-link ampstart-nav-link" href="#">LIÊN HỆ</a>
                    </li>
                </ul>
            </nav>
        </amp-sidebar>
        <!--top-->  
        <div class="home-three-wrapper">
            <!--Slider Area Start-->
            <div class="slider-area-home-three">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="preview-2">
                                <div id="nivoslider" class="slides">	
                                    <a href="#"><amp-img class="w3-image" src="<?= base_url() ?>img/slider/slider-1.jpg" width="1000px" height="400px" alt="" title="#slider-1-caption1"/></amp-img></a>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Slider Area-->
            <!--Timer Product Carousel Area Start-->
            <div class="timer-product-carousel-area">
                <div class="container">
                    <div class="section-padding">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-title"><h2>Mẫu Mới</h2></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="product-carousel">
                                <?php
                                    $a_sid=array();
                                    if(isset($_SESSION['ss'])) {
                                        foreach($_SESSION['ss'] as $k=>$v)
                                        {
                                            $a_sid[$k]=$v['ssid'];
                                        }
                                    }
                                    for ($j = 0, $count_spmoi = count($productweb); $j < $count_spmoi; $j++) { ?> 
                                <div class="col-lg-12">
                                    <!--item 1 start-->
                                    <div class="single-product-item">
                                        <div class="single-product clearfix">
                                            <a href="<?= base_url(). $productweb[$j]["tenkhongdau_$lang"] ?>">
                                                <span class="product-image">
                                                    <amp-img class="w3-image" width="150px" height="200px" alt="<?= $productweb[$j]['tenkhongdau_vi'] ?>" src="<?= base_url() ?>thumb/540x728/1/<?php
                                                            if ($productweb[$j]['photo'] != NULL)
                                                                        echo _upload_product_l . $productweb[$j]['photo'];
                                                                else
                                                                    echo 'images/no-image-available.png'; ?>"
                                                                    alt="<?= $productweb[$j]["ten_$lang"] ?>" /></amp-img>
                                                </span>
                                            </a>
                                        </div>
                                        <h2 class="single-product-name"><a href="<?= base_url(). $productweb[$j]["tenkhongdau_$lang"] ?>"><?= $productweb[$j]["ten_$lang"] ?></a></h2>
                                        
                                        <div class="price-box">
                                            <p class="old-price">
                                                <span class="price"><?=$productweb[$j]["gia_vnd"] ?> vnđ</span>
                                            </p>
                                            <p class="special-price">
                                                <span class="price" style="color:#f36e25"><?= get_price($productweb[$j]["id"]) ?> vnđ</span>
                                            </p> 											
                                        </div>
                                        
                                    </div><!--end of item 1-->
                                </div><!--end of col-md-3-->
                                <?php } ?>
                                
                                

                            </div>
                        </div>
                    </div>    
                </div>
            </div>
            <!--End of Timer Product Carousel Area-->
        </div>    
        <!--End of Page Wrapper-->
        
        <footer class="footer footer-home-three">
            <div class="container">
                <div class="footer-widget-padding">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget mb-sm-30">
                                <div class="footer-widget-title">
                                    <h3>Về Mykids Store</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <ul>
                                        <li><a href="#">Giới thiệu Mykids Store</a></li>
                                        <li><a href="#">Tuyển dụng khối văn phòng</a></li>
                                        <li><a href="#">Tuyển dụng khối siêu thị</a></li>
                                        <li><a href="#">Chính sách bảo mật</a></li>
                                        <li><a href="#">Điều khoản sử dụng</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget mb-sm-30">
                                <div class="footer-widget-title">
                                    <h3>Hỗ trợ khách hàng</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <ul>
                                        <li><a href="#">Mua & giao nhận online</a></li>
                                        <li><a href="#">Qui định & hình thức thanh toán</a></li>
                                        <li><a href="#">Bảo hành & bảo trì</a></li>
                                        <li><a href="#">Đổi trả & hoàn tiền</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget">
                                <div class="footer-widget-title">
                                    <h3>Đơn vị vận chuyển</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <div class="van-chuyen row">
                                        <div class="item-logo col-6"><a><amp-img class="w3-image" src="img/footer/ahamove.png" width="100px" height="50px"></amp-img></a></div>
                                        <div class="item-logo col-6"><a><amp-img class="w3-image" src="img/footer/grab-express.png" width="100px" height="50px"></amp-img></a></div>
                                        <div class="item-logo col-6"><a><amp-img class="w3-image" src="img/footer/viettel.png" width="100px" height="50px"></amp-img></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-12">
                            <div class="single-widget">
                                <div class="footer-widget-title">
                                    <h3>Cách thức thanh toán</h3>
                                </div>
                                <div class="footer-widget-list ">
                                    <div class="thanh-toan row">
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/cod.png" width="60px" height="30px" ></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/atm.png" width="60px" height="30px"></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/vnpay.png" width="60px" height="30px"></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/visa.png" width="60px" height="30px"></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/master.png" width="60px" height="30px"></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/jcb.png" width="60px" height="30px"></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/zalopay.png" width="60px" height="30px"></amp-img></a></div>
                                        <div class="item-logo col-4"><a><amp-img class="w3-image" src="img/footer/momo.png" width="60px" height="30px"></amp-img></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="footer-padding">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-7 d-flex">
                            <div class="footer-logo">
                                <a href="index"><img class="img-fluid" src="img/logo/logo.png" alt=""></a>
                            </div>
                            <div class="footer-widget-list">
                                <h2 style="margin-left: 25px;color:#000;">MY KIDS STORE</h2>
                                <ul class="address">
                                    <li><span class="fa fa-phone fa-fw"></span><strong> Điện thoại: </strong><?=$row_setting['hotline']?></li>
                                    <li><span class="fa fa-envelope-o fa-fw"></span><strong> Email: </strong><?=$row_setting['email']?></li>
                                    <li><span class="fa fa-map-marker fa-fw"></span><strong> Địa chỉ: </strong><?=$row_setting['diachi_vi']?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-2">
                            <a href="">
                                <img src="img/footer/noticed.png" alt="">
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 text-right">
                            <div class="footer-widget-title">
                                <h3>Kết nối với chúng tôi</h3>
                            </div>
                            <div class="footer-widget-list float-right">
                                <ul class="social-link">
                                    <li><a target="_blank" href="https://www.facebook.com/mykidsstoreHCM"><i class="icon icon-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon icon-youtube"></i></a></li>
                                    <li><a href="#"><i class="icon icon-instagram"></i></a></li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>

